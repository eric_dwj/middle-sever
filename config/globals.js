
 /**
  * Global Variable Configuration
  * (sails.config.globals)
  *
  * Configure which global variables which will be exposed
  * automatically by Sails.
  *
  * For more information on configuration, check out:
  * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.globals.html
  */
 module.exports.globals = {
 
    //以太链环境设置  start 
    //交易的gas费
    eth_transfer_gasLimit        :42000,
    //Token交易的gas费
    token_transfer_gasLimit      :60000,
    //web3的接入点
    web3_http_provider           : 'https://kovan.infura.io/FrDFhx3FbezOwQJjQv9T',
    //eth的存币交易费用
    eth_receive_transferfee      :0.0006,
    //eth的支付交易费用
    eth_send_transferfee         :0.0006, 
    //eth的最小充值费用
    eth_receive_threshold        :0.01,
    //eth的最小充值费用这个费用会被扣除
    eth_send_threshold           :0.01,
 
    //Token的存币交易费用
    token_receive_transferfee    :0.001,
    //ETH入账地址
    eth_in_address               : '0xAfC28904Fc9fFbA207181e60a183716af4e5bce2',
    //ETH出账地址
    eth_out_address              : '0xAfC28904Fc9fFbA207181e60a183716af4e5bce2',
    //ETH出账私钥
    eth_out_privatekey           : 'aa131fa63c03c6afce225a3e20afad28d1ba7d97ddbad9067e392d7e64847e9e',
    //token elot address
    token_elot_address           : '0xaaf4e93a3dcfad3ce0af9230f2ba0a73ae98c45d',
    //token elot address
    token_eos_address            : '0xad9500b20fac64a8124258c84ddc1edea0ae1007',
    //token入账地址
    token_in_address             : '0xA47EA6881A41F3ce5e610478132f8fd8F03EF198',
    //token elot的最小充值费用
    token_elot_receive_threshold : '10',
     //token elot的最小充值费用
    token_eos_receive_threshold  : '10',
 
    //token elot的最小支付费用这个费用会被扣除
    token_elot_send_threshold    : '10',
    token_eos_send_threshold     : '10',
    token_send_transferfee       :  0.0006,
    token_out_privatekey         :  '762D0D2F785888EF98E7E66A851951E4650C18B94480A994038C2F41FFBC7F18',
 
    erc20Interface_json          : 'assets/contract/ERC20Interface.json',
    
    token_confirm_block_number   : 4,
    eth_confirm_block_number     : 4,
    //token elot and eox address  
    token_elot_decimal           : 1,
    
    token_eos_decimal            : 100000000,
    //以太链环境设置  end
    
    //LTC环境设置 start
    sochain_ltc                   : { 
      url:'https://chain.so/',
      is_address_valid:'api/v2/is_address_valid/',
      get_info:'api/v2/get_info/',
      get_tx_unspent:'api/v2/get_tx_unspent/',
      get_address_balance:'api/v2/get_address_balance/',
      send_tx:'api/v2/send_tx/',
      testnetwork:'LTCTEST/',
      network:'LTC/',
      get_tx:'api/v2/get_tx/'
    },
    
    //LTC环境设置 end
    //比特币环境设置  start 
    //btc的当前网络 0 testnet 1: main net
    btc_current_net              :0,
    //btc的收款交易费用
    btc_receive_transferfee      :0.0001,
    //eth的最小充值费用
    btc_receive_threshold        :0.001,
    //btc的最小支付费用这个费用会被扣除
    btc_send_threshold           :0.001,
    //btc的支付交易费用
    btc_send_transferfee         :0.0001,          
    //BTC入账地址
    btc_in_address               :'myyn6Tw9UpcdM7LCos7qVjWi31Fxrr2GzG',
    //BTC出账地址
    btc_out_address              :'myyn6Tw9UpcdM7LCos7qVjWi31Fxrr2GzG',
    //BTC出账私钥  
    //cR7a7HAzq7XbEHXJ9BuYou1pUghwhBhTUcAVLeirDpsryEaZ2x2q 
    //mvN6B6c94wu7K46A36mFARFtsni2bssKeN 
    btc_out_privatekey           :'cQnaonFUexs6vXrzjEeyphJFpdL9KP9NKNHFTYEuYEk3fSzaYqN8',
    //bitcoind的服务器地址配置
    bitcore                      : { 
                                     url:'http://150.95.184.57:3001/',
                                     is_address_valid:'insight-api/addr/',
                                     get_info:'insight-api/status?q=getInfo',
                                     get_tx_unspent:'/utxo',
                                     get_address_balance:'insight-api/addr/',
                                     send_tx:'insight-api/tx/send',
                                     get_tx:'insight-api/tx/'
                                   },
 
    btc_confirm_block_number     :2
    //比特币环境设置  end
 };


