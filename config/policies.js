/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions (`true` allows public     *
  * access)                                                                  *
  *                                                                          *
  ***************************************************************************/

  '*': ['isAuthorized'], // Everything resctricted here
  'UsersController': {
    'create': true, // We dont need authorization here, allowing public access
    'bip39accounts': true,
    'importbip39accountsbymnemonic':true,
    'importbip39accountsbyseedhex':true,
    'encryptwallet':true,
    'decryptwallet':true,
    'createBatch':true
    
  },
   'ExchangeBalanceController': {
    '*': true // We dont need authorization here, allowing public access
  },

  'TradeController': {
    '*': true // We dont need authorization here, allowing public access
  },
  'SendController': {
    '*': true // We dont need authorization here, allowing public access
  },

  'ReceiveController': {
    '*': true // We dont need authorization here, allowing public access
  },

  'AuthController': {
    '*': true // We dont need authorization here, allowing public access
  },

  'ConfigController': {
    '*': true // We dont need authorization here, allowing public access
  },

  'EmailsenderController': {
    '*': true // We dont need authorization here, allowing public access
  }
  ,

  'EmailGerneralService': {
    '*': true // We dont need authorization here, allowing public access
  },

  'EmailVerifyController': {
    '*': true // We dont need authorization here, allowing public access
  },
  
  'GeneralDataController': {
    '*': true // We dont need authorization here, allowing public access
  },

  'GeneralMetaDataController': {
    '*': true // We dont need authorization here, allowing public access
  },
  
  'CaptchapngController': {
    '*': true // We dont need authorization here, allowing public access
  },

  'APIKeyController': {
    '*': true, // We dont need authorization here, allowing public access
  },
  'EntrypointController': {
    '*': true, // We dont need authorization here, allowing public access
    'create': 'isApikeyAuth'
  },


  'TransferController': {
    '*': true // We dont need authorization here, allowing public access
  }
  // '*': true,

  /***************************************************************************
  *                                                                          *
  * Here's an example of mapping some policies to run before a controller    *
  * and its actions                                                          *
  *                                                                          *
  ***************************************************************************/
	// RabbitController: {

		// Apply the `false` policy as the default for all of RabbitController's actions
		// (`false` prevents all access, which ensures that nothing bad happens to our rabbits)
		// '*': false,

		// For the action `nurture`, apply the 'isRabbitMother' policy
		// (this overrides `false` above)
		// nurture	: 'isRabbitMother',

		// Apply the `isNiceToAnimals` AND `hasRabbitFood` policies
		// before letting any users feed our rabbits
		// feed : ['isNiceToAnimals', 'hasRabbitFood']
	// }
};
