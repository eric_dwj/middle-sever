module.exports.trader = {
   trade_server                 : 'http://150.95.176.60:8080',
   global_price                 : 'https://api.coinmarketcap.com/v2/ticker/?limit=10',
   order_limit                  : 'order.put_limit',
   balance_update               : 'balance.update',
   balance_query                : 'balance.query',
   order_pending                : 'order.pending',
   order_cancel                 : 'order.cancel',
   order_finished               : 'order.finished',
   order_pending                : 'order.pending',
   order_finished_detail        : 'order.finished_detail',
   order_pending_detail         : 'order.pending_detail',
   order_deals                  : 'order.deals',
   market_user_deals            : 'market.user_deals',
   order_book                   : 'order.book',
   business_deposit             : 'deposit',
   business_withdraw            : 'withdraw',
   trade_external_flag          : 1,
   trade_internal_flag          : 2,
   market                       : ['EOSBTC','EOSETH','ELOTETH','ELOTBTC','ETHBTC'],
   market_param                 : {
                                    EOSBTC:{asset:'EOS',money:'BTC'},
                                    EOSETH:{asset:'EOS',money:'ETH'},
                                    ELOTETH:{asset:'ELOT',money:'ETH'},
                                    ELOTBTC:{asset:'ELOT',money:'BTC'},
                                    ETHBTC:{asset:'ETH',money:'BTC'}
                                  },
   pairs                        : ['EOS-BTC','EOS-ETH','ELOT-ETH','ELOT-BTC','ETH-BTC']
};