module.exports.redis = {

   redis_url                    :'127.0.0.1',
   
   redis_port                   : 6379,
   
   mq_onreceive_process_check   : 'mq_onreceive_process_check',
   //Redis的hashkeys start
   //hashkey:field:value  => asset_history_hashkey_2018-09-26:userid:txid
   asset_history_redis_hashkey          : 'asset_history_hashkey',
 
   asset_redis_hashkey                  : 'asset_hashkey',

   trade_asset_history_redis_hashkey    : 'trade_asset_history_hashkey',

   trade_asset_redis_hashkey            : 'trade_asset_hashkey',

   order_redis_hashkey                  : 'order_redis_hashkey',

   order_confirm_redis_hashkey          : 'order_confirm_redis_hashkey'
};