const request = require('request');
const url_str = sails.config.trader.trade_server;  

var JsonRPCUtil = function(){};


JsonRPCUtil.Get = function(url){
   
   
	return new Promise((resolve, reject) => {

            var headers = {"content-type": "application/json"};
            var options = {
                            url: url,
                            method: 'GET',
                            headers: headers
                        };              
           
            request.get(options, (error, response, body)=> {
                if (error) {
                    return reject(error);
                }else {
                    return resolve(body);
                }
            });

	});
}

JsonRPCUtil.PostV2 = function(jsondata,url){
    console.log('jsondata',jsondata);
    return new Promise((resolve, reject) => {

            var headers = {"content-type": "application/json"};
            var options = {
                            url: url,
                            method: 'POST',
                            headers: headers,
                            json: jsondata
                        };              
           
            request.post(options, (error, response, body)=> {
                console.log('body',body);
                if (error) {
                    return reject(error);
                }else {
                    return resolve(body);
                }
            });

    });

} 

JsonRPCUtil.Post = function(jsondata,url){
   
    if(!url)
    {
    	url = url_str;
    }

	return new Promise((resolve, reject) => {

            var headers = {"content-type": "application/json"};
            var options = {
                            url: url,
                            method: 'POST',
                            headers: headers,
                            json: jsondata
                        };              
           
            request.post(options, (error, response, body)=> {
                
                if (!body) {
                    return reject(error);
                }
                if (body.error) {
                    var error = {};
                    error.message = body.error;
                    return reject(error);
                } else {
                    return resolve(body);
                }
            });

	});
}

module.exports = JsonRPCUtil;