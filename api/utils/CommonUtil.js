let JsonRPCUtil  = require('../utils/JsonRPCUtil');
var Decimal    = require('decimal.js');
var QRCode     = require('qrcode');
var scheduler  = require('node-schedule');
var CryptoJS   = require("crypto-js");
var CommonUtil = function(){}

CommonUtil.getTicker = function(){
     return new Promise(resolve=>{
        JsonRPCUtil.Get(sails.config.trader.global_price)
        .then((ticker)=>{

            resolve(ticker);
        })
     })
}


CommonUtil.sleep = function(ms){
     return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
}

CommonUtil.add  = function(x,y){
    return Decimal.add(x,y);
}

CommonUtil.subtract  = function(x,y){
    return Decimal.sub(x,y);
}

CommonUtil.multiply  = function(x,y){
    return Decimal.mul(x, y);
}

CommonUtil.divide  = function(x,y){
    return Decimal.div(x, y);
}

CommonUtil.encrypt = function( data , password ){
    return new Promise((resolve,reject)=>{
        let encryptedkeystore;

        try
        {
            encryptedkeystore = CryptoJS.AES.encrypt( data , password );
        }
        catch(exception)
        {
            reject(exception);
            sails.log.error(exception);
        }

        resolve(encryptedkeystore) ;
    })   
}

CommonUtil.decrypt = function( data , password ){

    return new Promise((resolve,reject)=>{

        let keyStore;
        try{
            var bytes  = CryptoJS.AES.decrypt( data, password );
            keyStore = bytes.toString(CryptoJS.enc.Utf8);
        }
        catch(exception){
            sails.log.error("Maybe Password is wrong: exception:",exception);
            reject(exception);
        }
        resolve(keyStore);
    })

}

/**
 * 为各种数据生成base64的二维码，并且返回
 */
CommonUtil.qrcode = function(data){
	sails.log.info("CommonUtil.qrcode : data "+ data );
	return new Promise((resolve, reject) => {
		QRCode.toDataURL(data, function (err, url) {
          resolve(url);
        });
	});
}

CommonUtil.scheduler = function(cron,callback,callbackname){
	sails.log.debug("CommonUtil.scheduler "+cron+": callback  "+ callbackname );
	scheduler.scheduleJob(cron, callback);
}


CommonUtil.isJSON = function (str) {
    //sails.log.info("CommonUtil.isJSON str: "+str );
        if (typeof str == 'string') {
            try {
                var obj=JSON.parse(str);
                
                return true;
            } catch(e) {
                
                return false;
            }
        }
 }　

//获取当前时间，格式YYYY-MM-DD
CommonUtil.getNowFormatDate = function () {
	sails.log.info("CommonUtil.getNowFormatDate " );
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
}

//获取昨天时间，格式YYYY-MM-DD
CommonUtil.getYesterdayFormatDate = function () {
	sails.log.info("CommonUtil.getYesterdayFormatDate " );
        var date = new Date();
        date.setDate(date.getDate()-1);
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var yesterday = year + seperator1 + month + seperator1 + strDate;
        return yesterday;
}



module.exports = CommonUtil