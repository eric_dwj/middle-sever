/**
* ETH调用API的工具包
*/

var Web3      = require('web3');
var web3      = new Web3( new Web3.providers.HttpProvider(sails.config.globals.web3_http_provider));
var fs        = require('fs');
var Tx        = require('ethereumjs-tx');
var gasLimit  = sails.config.globals.eth_transfer_gasLimit;
let RedisUtil = require('../utils/RedisUtil');
let CommonUtil= require('../utils/CommonUtil');

var ETHUtil = function(){}

ETHUtil.getCurrentBlock = function()
{
   return web3.eth.getBlockNumber();
}

ETHUtil.validateaddress = function(address)
{
  return new Promise((resolve,reject)=>{
    resolve(web3.utils.isAddress(address));
  })
}


ETHUtil.getTransactionCount = function(address)
{
    return web3.eth.getTransactionCount( address );
}

ETHUtil.createRawTransaction = function(from,to,pk,gasPrice,amount){
    return new Promise((resolve, reject) => {
        if( !from )
        {
            reject("from_address_not_existed")
        }

        if( !to )
        {
            reject("to_address_not_existed")
        }

        if( !pk )
        {
            reject("pk_address_not_existed")
        }

        if( !gasPrice )
        {
            reject("gasPrice_address_not_existed")
        }

        if( !amount )
        {
            reject("amount_address_not_existed")
        }

        let pkbuf = new Buffer(pk, 'hex');

        web3.eth.getTransactionCount(from).then((txCount) =>{ 
            var rawTx = {
                            nonce: web3.utils.toHex(txCount),
                            gasLimit: web3.utils.toHex(gasLimit),
                            gasPrice:web3.utils.toHex(parseInt(CommonUtil.multiply(gasPrice, 1e9))),
                            to: to,
                            from: from, 
                            value: parseInt(CommonUtil.multiply(amount, 1e18))
                          };

            // console.log('parseInt(CommonUtil.multiply(gasPrice, 1e9))',parseInt(CommonUtil.multiply(gasPrice, 1e9)));  
            // console.log('CommonUtil.multiply(amount, 1e18)',parseInt(CommonUtil.multiply(amount, 1e18)));            

          var tx = new Tx(rawTx);
          tx.sign(pkbuf);
          var serializedTx = tx.serialize();
          var transactionData = '0x' + serializedTx.toString('hex');
          resolve(transactionData);
        });
    });
}

ETHUtil.sendTransaction = function(transactionData){
   
    return new Promise((resolve, reject) => {
         web3.eth.sendSignedTransaction( transactionData , function(err, hash) {
    
            if (err) 
            {
              reject(err);
            }else
            {
              resolve(hash);
            }

        });
    });
}

ETHUtil.getAddressFromPk = function(pk){
    sails.log.info("ETHUtil.getAddressFromPk: PK "+pk ); 
    return web3.eth.accounts.privateKeyToAccount("0x"+pk).address;
}

ETHUtil.getBalance = function(address){
    //sails.log.info("ETHUtil.getBalance: address "+ address ); 
    return new Promise((resolve, reject) => {
        web3.eth.getBalance(address)
        .then((balance)=>{
            resolve(web3.utils.fromWei(balance))
        });
    });
}



//获取Gwei单位的gas price，这个是转账交易费的；
ETHUtil.getReceiveGasPriceByGwei = function(){
    return  parseFloat(CommonUtil.divide(CommonUtil.multiply(sails.config.globals.eth_receive_transferfee,1e9),sails.config.globals.eth_transfer_gasLimit));
}

ETHUtil.getSendGasPriceByGwei = function(){
    return  parseFloat(CommonUtil.divide(CommonUtil.multiply(sails.config.globals.eth_send_transferfee,1e9),sails.config.globals.eth_transfer_gasLimit));
}

//将转帐到ETH钱包总账的hash值，保存到redis里面；
ETHUtil.saveAssetHistoryToRedis = function(field,flag){
    var Deposit2TotalHashkey = sails.config.redis.eth_to_total_redis_hashkey +"_"+ CommonUtil.getNowFormatDate();
    sails.log.info("ETHUtil.saveAssetHistoryToRedis: hashkey"+Deposit2TotalHashkey
        +" field "+field 
        +" true or false flag: " + flag ); 
    
    return RedisUtil.hset(Deposit2TotalHashkey,field,flag);
    
}

//获取转账详情
ETHUtil.getTransactionReceipt = function(hash){
    return web3.eth.getTransactionReceipt(hash);
}

//ETH转账
ETHUtil.transfer = function(from,to,pk,gasPrice,amount){
    return  new Promise((resolve, reject) => {
           ETHUtil.createRawTransaction(from,to,pk,gasPrice,amount)
           .then((rawTransactionData)=>{
               ETHUtil.sendTransaction(rawTransactionData)
               .then((hash)=>{
                  resolve(hash);
               })
               .catch((exception)=>{reject(exception)})
               ;
           })
           .catch((exception)=>{reject(exception)});
         });
}


module.exports = ETHUtil