
let RedisUtil   = require('../utils/RedisUtil');
let DBUtil      = require('../utils/DBUtil');
let CommonUtil  = require('../utils/CommonUtil');

var AssetUtil = function(){}

AssetUtil.getAllAssetHistoryFromRedis = function( hashkey ){
	if(    hashkey == null 
		|| hashkey == undefined 
		|| hashkey == "undefined" 
		|| hashkey == "null" )
	{
		return {err:'hashkey_null',hashkey:hashkey};
	}
	return RedisUtil.hkeys( hashkey );
}

AssetUtil.updateAssetHistory = function( id ,state){
	return DBUtil.updateAssetHistory(id,{state:state})
}

AssetUtil.updateAssetSend = function( userid , asset , assetAvailableName){
    return new Promise((resolve,reject)=>{
   			DBUtil.findAsset(userid).then((record)=>{
		if( record ==undefined || record =='undefined' )
		{
			reject('asset_record_not_found')
		}
		else
		{

			console.log('asset',asset);
			console.log('assetAvailableName',assetAvailableName);
			if( asset && asset[assetAvailableName] )
			{
				record[assetAvailableName] = CommonUtil.subtract(record[assetAvailableName],asset[assetAvailableName]);
			}
	
			DBUtil.updateAsset(userid,record).then((result)=>{	
				resolve(result);
			})
			.catch((err)=>{
             sails.log.error("err",err);
			});
		}
	})
  })
}

AssetUtil.updateAssetReceive = function( userid,asset,assetAvailableName){
    
    return new Promise((resolve,reject)=>{
   			DBUtil.findAsset(userid).then((record)=>{
		if( record ==undefined || record =='undefined' )
		{
			DBUtil.createAsset(asset)
			.then((result)=>{
				resolve(result);
			});
		}
		else
		{
			if( asset && asset[assetAvailableName] )
			{
				record[assetAvailableName] = CommonUtil.add(record[assetAvailableName],asset[assetAvailableName]);
			}

			DBUtil.updateAsset(userid,record).then((result)=>{	
				resolve(result);
			})
			.catch((err)=>{
             sails.log.error("err",err);
			});
		}
	})
  })
}

AssetUtil.createAsset = function( userid ){
	return DBUtil.createAsset({userid:userid});
}

AssetUtil.initAssetHistoryUnchecked = function( userid , asset , amount,txid,side,state ){
   if( userid == null 
   	|| userid == undefined 
   	|| userid == 'undefined' 
   	|| userid == 'null' )
	{
		return {err:'userid_null'};
	}

	if( asset == null 
   	|| asset == undefined 
   	|| asset == 'undefined' 
   	|| asset == 'null' )
	{
		return {err:'asset_null'};
	}

   if( amount == null 
   	|| amount == undefined 
   	|| amount == 'undefined' 
   	|| amount == 'null' )
	{
		return {err:'amount_null'};
	}

	 if( txid == null 
   	||   txid == undefined 
   	||   txid == 'undefined' 
   	||   txid == 'null' )
	{
		return {err:'amount_null'};
	}

	if( side == null 
   	||   side == undefined 
   	||   side == 'undefined' 
   	||   side == 'null' )
	{
		return {err:'side_null'};
	}

	if( state == null 
   	||  state == undefined 
   	||  state == 'undefined' 
   	||  state == 'null' )
	{
		return {err:'state_null'};
	}



   var assetHistoryRecord = {};
   assetHistoryRecord.userid    = userid;
   assetHistoryRecord.asset     = asset;
   assetHistoryRecord.amount    = parseFloat(amount);
   assetHistoryRecord.timestamp = new Date().getTime();
   assetHistoryRecord.txid      = txid;
   assetHistoryRecord.side		= side; 
   assetHistoryRecord.state     = state;

   return DBUtil.createAssetHistory(assetHistoryRecord);
}

AssetUtil.updateAssetInRedis = function(userid,asset)
{
	return new Promise((resolve,reject)=>{
		if( userid == null 
	   	|| userid  == undefined 
	   	|| userid  == "undefined" 
	   	|| userid  == "null" )
		{
			reject({err:'userid_null'});
			return ;
		}

		if( asset == null 
	   	|| asset  == undefined 
	   	|| asset  == "undefined" 
	   	|| asset  == "null" )
		{
			reject({err:'asset_null'});
			return ;
		}

		RedisUtil.hset( sails.config.redis.asset_redis_hashkey,userid,JSON.stringify(asset))
    	.then(( flag )=>{    
    			resolve( flag );
    	});


	});
}

AssetUtil.updateAssetHistoryInRedis = function(userid,assetHistory,assetHistoryDate,flag)
{
	return new Promise((resolve,reject)=>{
		if( userid == null 
	   	|| userid  == undefined 
	   	|| userid  == "undefined" 
	   	|| userid  == "null" )
		{
			reject({err:'userid_null'});
			return ;
		}

		if( assetHistory == null 
	   	|| assetHistory  == undefined 
	   	|| assetHistory  == "undefined" 
	   	|| assetHistory  == "null" )
		{
			reject({err:'assetHistory_null'});
			return ;
		}

		if( flag == null 
	   	|| flag  == undefined 
	   	|| flag  == "undefined" 
	   	|| flag  == "null" )
		{
			reject({err:'flag_null'});
			return ;
		}

		if( assetHistoryDate == null 
	   	|| assetHistoryDate  == undefined 
	   	|| assetHistoryDate  == "undefined" 
	   	|| assetHistoryDate  == "null" )
		{
			reject({err:'assetHistoryDate_null'});
			return ;
		}

		RedisUtil.hset( sails.config.redis.asset_history_redis_hashkey+'_'+userid,JSON.stringify(assetHistory),flag)
    	.then(( resp1 )=>{   
			RedisUtil.hset( sails.config.redis.asset_history_redis_hashkey+'_'+assetHistoryDate,JSON.stringify(assetHistory),flag)
	    	.then(( resp2 )=>{   
	    			resolve( resp2 );
	    	});
    	});


	});
}

AssetUtil.getAssetHistoryFromRedis = function( hashkey , field ){
    return new Promise((resolve,reject)=>{
	    if( hashkey == null 
	   	|| hashkey == undefined 
	   	|| hashkey == "undefined" 
	   	|| hashkey == "null" )
		{
			reject({err:'hashkey_null'});
			return ;
		}

		if( field == null 
	   	|| field == undefined 
	   	|| field == "undefined" 
	   	|| field == "null" )
		{
			reject({err:'field_null'});
			return ;
		}

    	RedisUtil.hget( hashkey , field )
    	.then(( flag )=>{
    		    
    			resolve( flag );
    			
    	});
    });
}


module.exports = AssetUtil;