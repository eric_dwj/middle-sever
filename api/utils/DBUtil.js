var DBUtil = function(){};
DBUtil.loadAssetTx = function(condition){
    return new Promise((resolve,reject)=>{
        AssetTx.find( condition )
        .exec(function ( err , records) {
                if(err){
                    reject({err:err,res:"error"});
                }else{
                    resolve(records);
                }
        });
    })

}

DBUtil.createAssetTx= function(assetTx){
    return new Promise((resolve,reject)=>{
        AssetTx.create( assetTx )
        .exec(function ( err , records) {
                if(err){
                    reject({err:err,res:"error"});
                }else{
                    resolve(records);
                }
        });
    })
}


DBUtil.updateAssetTx= function(tx,status){
  return new Promise((resolve,reject)=>{
     AssetTx.update( tx, status )
     .exec(function ( err , records) {
            if(err){
                reject({err:err,res:'error'});
            }else{   
                resolve( records[0] );
            }
        });
  })
}

DBUtil.updateOrder = function( orderid , orderObj ){
    return new Promise((resolve, reject) => {
        Order.update( {orderid:String(orderid) }, orderObj )
        .exec(function ( err , records) {
            if(err){
                reject({err:err,res:'error'});
            }else{   
                resolve( records[0] );
            }
        });
    });
}

DBUtil.createOrder = function(order){
    return new Promise((resolve, reject) => {
        Order.create( order )
        .exec(function ( err , records) {
            if(err){
                reject({err:err,res:"error"});
            }else{
                resolve(records);
            }
        });
    });
}

DBUtil.createTradeAssetHistory = function(assetHistory){
    return new Promise((resolve, reject) => {
            TradeAssetHistory.create( assetHistory )
            .exec(function (err, records) {
                if(err){
                    reject({err:err,res:'error'});
                }else{
                    resolve(records);
                }
            });

    });

}


DBUtil.loadUserData = function(condition){
	return new Promise((resolve, reject) => {
		Users.find(condition)
	    .exec(function (err, records) {
	    	if(err){
            		reject({err:err,res:'error'});
            }else{
            		resolve(records);
            }
	    });
	});
	

}

DBUtil.findTradeAsset = function(userid){

    return new Promise((resolve, reject) => {

        TradeAsset.findOne( { userid : userid } )
        .exec(function (err, asset) {
          if (err) {
            reject(err);
          }else {
            resolve(asset);
          }
        });

    });
}

DBUtil.findAsset = function(userid){

	return new Promise((resolve, reject) => {

		Asset.findOne( { userid : userid } ).exec(function (err, asset) {
          if (err) {
            reject(err);
          }else {
            resolve(asset);
          }
        });

	});
}

DBUtil.updateTradeAsset = function(userid,record){
    return new Promise((resolve, reject) => {
            TradeAsset.update( {userid:userid},record )
            .exec(function (err, records) {
                if(err){
                    reject({err:err,res:'error'});
                }else{
                    resolve(records);
                }
            });
    });
}

DBUtil.updateAsset = function(userid,record){
	return new Promise((resolve, reject) => {
		    Asset.update( {userid:userid},record )
            .exec(function (err, records) {
            	if(err){
            		reject({err:err,res:'error'});
            	}else{
            		resolve(records);
            	}
            });
	});
}


DBUtil.createTradeAsset = function(asset){
    return new Promise((resolve, reject) => {
            TradeAsset.create( asset )
            .exec(function (err, records) {
                if(err){
                    reject({err:err,res:'error'});
                }else{
                    resolve(records);
                }
            });

    });

}

DBUtil.createAsset = function(asset){
	return new Promise((resolve, reject) => {
		    Asset.create( asset )
            .exec(function (err, records) {
            	if(err){
            		reject({err:err,res:'error'});
            	}else{
            		resolve(records);
            	}
            });
	});
}

DBUtil.updateAssetHistory = function(id,record){
    return new Promise((resolve, reject) => {
            AssetHistory.update( {id:id},record )
            .exec(function (err, records) {
                if(err){
                    reject({err:err,res:'error'});
                }else{
                    resolve(records);
                }
            });
    });

}

DBUtil.createAssetHistory = function(assetHistory){
	return new Promise((resolve, reject) => {
		    AssetHistory.create( assetHistory )
            .exec(function (err, records) {
            	if(err){
            		reject({err:err,res:'error'});
            	}else{
            		resolve(records);
            	}
            });
	});
}

module.exports = DBUtil;