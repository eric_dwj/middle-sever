var CacheUtil = function(){}

const NodeCache = require( "node-cache" );
const Cache = new NodeCache({ stdTTL: 100, checkperiod: 120 });
CacheUtil.set = function(key,value){
	
	return new Promise((resolve, reject) => {
		Cache.set( key, value, function( err, success ){
	  			if( !err && success ){
	               resolve(success);
	  			}else
	  			{
	  			   reject(err)
	  			}
		});
	});
},

CacheUtil.get = function(key){

	//sails.log.info("CacheUtil.get : key:  "+ key);

	return new Promise((resolve, reject) => {
		var value = Cache.get(key);
		resolve(value);
	});

}

module.exports = CacheUtil