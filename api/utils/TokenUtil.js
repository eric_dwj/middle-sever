
var Web3      = require('web3');
var web3      = new Web3( new Web3.providers.HttpProvider(sails.config.globals.web3_http_provider));
var fs        = require('fs');
var erc20Path = require('path').resolve(sails.config.appPath, sails.config.globals.erc20Interface_json);
var erc20ABI  = JSON.parse(fs.readFileSync(erc20Path, 'utf8')).abi;
var Tx        = require('ethereumjs-tx');
var gasLimit  = sails.config.globals.token_transfer_gasLimit;
let RedisUtil = require('../utils/RedisUtil');
let CommonUtil= require('../utils/CommonUtil');
let ETHUtil   = require('../utils/ETHUtil');

var TokenUtil = function(){};

TokenUtil.getCurrentBlock = function()
{
   return web3.eth.getBlockNumber();
}

TokenUtil.validateaddress = function(address)
{
  return new Promise((resolve,reject)=>{
    resolve(web3.utils.isAddress(address));
  })
}

TokenUtil.getAddressFromPk = function(pk){ 
    return web3.eth.accounts.privateKeyToAccount("0x"+pk).address;
}

TokenUtil.getTransactionCount = function(address)
{
    return web3.eth.getTransactionCount( address );
}

//获取转账详情
TokenUtil.getTransactionReceipt = function(hash){
    return web3.eth.getTransactionReceipt(hash);
}

TokenUtil.createRawTransaction = function(from,to,pk,gasPrice,amount,contractaddress){
   
   
    return new Promise((resolve, reject) => {

        if( ! from )
        {
            reject("from_address_not_existed")
        }

        if( ! to )
        {
            reject("to_address_not_existed")
        }

        if( ! pk )
        {
            reject("pk_address_not_existed")
        }

        if( ! gasPrice )
        {
            reject("gasPrice_address_not_existed")
        }

        if( ! amount )
        {
            reject("amount_address_not_existed")
        }

        if( ! contractaddress )
        {
            reject("contractaddress_address_not_existed")
        }
      

        let pkbuf = new Buffer(pk, 'hex');
        
        var contract = new web3.eth.Contract( erc20ABI , contractaddress );
        var dataobj  = contract.methods.transfer( to , amount　).encodeABI();

        web3.eth.getTransactionCount(from).then((txCount) =>{ 

            var rawTx = {
                               nonce: web3.utils.toHex(txCount),
                            gasLimit: web3.utils.toHex(gasLimit),
                            gasPrice: web3.utils.toHex(parseInt(CommonUtil.multiply(gasPrice,1e9))),
                                  to: contractaddress,
                                from: from,
                                data: dataobj
                          };
          var tx = new Tx(rawTx);
          tx.sign(pkbuf);
          var serializedTx = tx.serialize();
          var transactionData = '0x' + serializedTx.toString('hex');
          resolve(transactionData);
        });
    });
}

//ETH转账
TokenUtil.transfer = function(from,to,pk,gasPrice,amount,contractaddress){
    return new Promise((resolve, reject) => {
           TokenUtil.createRawTransaction(from,to,pk,gasPrice,amount,contractaddress)
           .then((rawTransactionData)=>{
               ETHUtil.sendTransaction(rawTransactionData)
               .then((hash)=>{
                  resolve(hash);
               })
               .catch((exception)=>{reject(exception)})
               ;
           })
           .catch((exception)=>{reject(exception)});
         });
}

//将转帐到ETH钱包总账的hash值，保存到redis里面；
TokenUtil.setDepositHash2Redis = function(tokenName,field,flag){
    var Deposit2TotalHashkey = tokenName +"_"+ sails.config.redis.token_to_total_redis_hashkey +"_"+ CommonUtil.getNowFormatDate();
    sails.log.info("ETHUtil.setDepositHash2Redis: hashkey"+Deposit2TotalHashkey
        +" field "+field 
        +" true or false flag: " + flag 
        +" token name" + tokenName
        ); 
    
    return RedisUtil.hset(Deposit2TotalHashkey,field,flag);
    
}

//获取Gwei单位的gas price，这个是转账交易费的；
TokenUtil.getReceiveGasPriceByGwei = function(){
    return  parseFloat(CommonUtil.divide(CommonUtil.multiply(sails.config.globals.token_receive_transferfee,1e9),sails.config.globals.token_transfer_gasLimit));
}

TokenUtil.getSendGasPriceByGwei = function(){
    return  parseFloat(CommonUtil.divide(CommonUtil.multiply(sails.config.globals.token_send_transferfee,1e9),sails.config.globals.token_transfer_gasLimit));
}

TokenUtil.getBalance = function(address,contractAddress){
   var contract =new web3.eth.Contract( erc20ABI , contractAddress );
   return contract.methods.balanceOf( address ).call()
       
}


module.exports = TokenUtil;


