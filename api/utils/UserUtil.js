
var UserUtil = function(){}

UserUtil.userid = parseInt( String( new Date().getTime() ).substring(0,8)  );

UserUtil.generateUserID = function(){

    UserUtil.userid = UserUtil.userid + 1;
    console.log('UserUtil.userid',UserUtil.userid);
    return UserUtil.userid;

}

UserUtil.processInnerAccount = function(account)
{
	delete account.mnemonic;
	delete account.bip39Seed;
	delete account.bip32BTCRootKey;
	delete account.bip32USDTRootKey;
	delete account.bip32LTCRootKey;
	delete account.bip32ETHRootKey;
	delete account.bip32BCHRootKey

	delete account.ETHAccount.privateKey;
	delete account.ELOTAccount.privateKey;
	delete account.EOSAccount.privateKey;
	delete account.ETCAccount.privateKey;
	delete account.BTCAccount.privateKey;
	delete account.LTCAccount.privateKey;
	//delete account.BCHAccount.privateKey;
	delete account.USDTAccount.privateKey;


}



module.exports = UserUtil;