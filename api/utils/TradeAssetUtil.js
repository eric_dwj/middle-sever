let RedisUtil   = require('../utils/RedisUtil');
let DBUtil      = require('../utils/DBUtil');
let CommonUtil  = require('../utils/CommonUtil');
let MQUtil      = require('../utils/MQUtil');

var TradeAssetUtil = function(){};

TradeAssetUtil.loadAssetUTXO = function(asset){
    return DBUtil.loadAssetTx({asset:asset,status:sails.config.asset.tx_submitted}); 
}

TradeAssetUtil.loadAssetTx = function(userid){
    return DBUtil.loadAssetTx({userid:userid}) 
}

TradeAssetUtil.submitAssetTx = function( txObj,side ){
      var assetTx = {};
      assetTx.userid = txObj.userid;
      assetTx.asset  = txObj.assetname;
      assetTx.side   = side;
      assetTx.amount = txObj.size;
      assetTx.status = sails.config.asset.tx_submitted;
      assetTx.tx     = txObj.tx;
      return TradeAssetUtil.createAssetTx(assetTx);
}

TradeAssetUtil.addChangedInfoToMq = function(userid,asset,assetHistoryIn,assetHistoryOut,order,cancelOrder,finishedOrder,receiveObject){
    
    var changeInfo = {};

    if( userid ){
      changeInfo.userid = userid;
    }
    
    if( assetHistoryIn ){
      changeInfo.assetHistoryIn = assetHistoryIn;
    }

    if( assetHistoryOut ){
      changeInfo.assetHistoryOut = assetHistoryOut;
    }

    if( asset ){
      changeInfo.asset = asset;
    }

    if( order ){
      changeInfo.order = order;
    }

    if( cancelOrder ){
      changeInfo.cancelOrder = cancelOrder;
    }

    if( finishedOrder ){
      changeInfo.finishedOrder = finishedOrder;
    }

    if( receiveObject){
      changeInfo.receiveObject = receiveObject;
    }

    return MQUtil.sendMessage(sails.config.mq.change_mq_info,JSON.stringify(changeInfo));
   
}


TradeAssetUtil.addMatchResToMq = function(order){ 
      return MQUtil.sendMessage(sails.config.mq.order_mq_match,JSON.stringify(order));
}

TradeAssetUtil.getMatchResFromMq = function(vt){
    let resp;
    try{
      resp =MQUtil.receiveMessage(sails.config.mq.order_mq_match,vt);
    }catch(exception){
      sails.log.error("MQUtil.receiveMessage "+ sails.config.mq.order_mq_match );
      sails.log.error(exception);
    }
    return resp;
}

TradeAssetUtil.removeMatchResFromMq = function( id ){
    return MQUtil.deleteMessage(sails.config.mq.order_mq_match,id);
}


TradeAssetUtil.updateCancelAsset = function(userid,asset,assetFrozen,assetAvailable){

    return new Promise((resolve,reject)=>{

      DBUtil.findTradeAsset(String(userid)).then((record)=>{
         if( record ==undefined || record =='undefined' || record == null || record == 'null' ){
             reject({error:"system_error_userid_notfound"});
         }else {
            if( asset && asset[assetFrozen] ){
               record[assetAvailable] = CommonUtil.add(record[assetAvailable],asset[assetFrozen]);
               record[assetFrozen]    = CommonUtil.subtract(record[assetFrozen],asset[assetFrozen]);
            }

            DBUtil.updateTradeAsset(userid,record)
            .then((result)=>{ 
               resolve(result[0]);
            }).catch((err)=>{
                sails.log.error("err",err);
            });
      }})
    });
}

TradeAssetUtil.createAssetTx= function(assetTx){
  return new Promise((resolve,reject)=>{
    DBUtil.createAssetTx(assetTx).then((result)=>{ 
         resolve(result);
      }).catch((err)=>{
         sails.log.error("err",err);
         reject(err);
      });
  })
}

TradeAssetUtil.updateAssetTx= function(tx,status){
  return new Promise((resolve,reject)=>{
    DBUtil.updateAssetTx({tx:tx},{status:status}).then((result)=>{ 
         resolve(result);
      }).catch((err)=>{
         sails.log.error("err",err);
         reject(err);
      });
  })
}

TradeAssetUtil.createOrder = function( order ){ 
   return new Promise((resolve,reject)=>{

      var orderObj={};
      orderObj.orderid   = order.id;
      orderObj.takerfee  = order.taker_fee;
      orderObj.makerfee  = order.maker_fee;
      orderObj.dealstock = order.deal_stock;
      orderObj.dealmoney = order.deal_money;
      orderObj.dealfee   = order.deal_fee;
      orderObj.ctime     = order.ctime;  
      orderObj.user      = order.user;
      orderObj.market    = order.market;
      orderObj.source    = order.source;
      orderObj.type      = order.type;
      orderObj.side      = order.side;
      orderObj.price     = order.price;
      orderObj.amount    = order.amount;
      orderObj.left      = order.left;

      DBUtil.createOrder(orderObj).then((result)=>{ 
         resolve(result);
      }).catch((err)=>{
         sails.log.error("err",err);
         reject(err);
      });
   });
}


TradeAssetUtil.updateOrderFinish = function( orderid , order ){
  //   left: '1.5',
  // deal_stock: '0',
  // deal_money: '0',
  // deal_fee: '0',
  
  let order2Update = {};
  if( order.left != undefined && order.left != 'undefined' && order.left != null && order.left != 'null' ){
    order2Update.left = order.left;
    if( order.left == '0e-8' ){
      order2Update.state = sails.config.order.order_state_finished;
    }
  }else{
    order2Update.left  = '0e-8';
    order2Update.state = sails.config.order.order_state_finished;
  }

  if( order.deal_stock != undefined && order.deal_stock != 'undefined' && order.deal_stock != null && order.deal_stock != 'null' ){
    order2Update.dealstock = order.deal_stock;
  }

  if( order.deal_money != undefined && order.deal_money != 'undefined' && order.deal_money != null && order.deal_money != 'null' ){
    order2Update.dealmoney = order.deal_money;
  }

  if( order.deal_fee != undefined && order.deal_fee != 'undefined' && order.deal_fee != null && order.deal_fee != 'null'  ){
    order2Update.dealfee = order.deal_fee;
  }
  return DBUtil.updateOrder( orderid, order2Update );
}

TradeAssetUtil.updateOrderCancel = function( orderid ){
  var orderCancel = {};
  orderCancel.state = sails.config.order.order_state_cancel;
  orderCancel.left  = '0e-8';
  return DBUtil.updateOrder( orderid , orderCancel );
}

TradeAssetUtil.updateOrderInRedis = function(userid,orderid,order){

   return new Promise((resolve,reject)=>{

      if(   userid  == null 
         || userid  == undefined 
         || userid  == 'undefined' 
         || userid  == 'null' ){       
         return reject({err:'userid_null'});
      }

      if(   orderid  == null 
         || orderid  == undefined 
         || orderid  == 'undefined' 
         || orderid  == 'null' ){       
         return reject({err:'orderid_null'});
      }


      if(   order  == null 
         || order  == undefined 
         || order  == 'undefined' 
         || order  == 'null' ){       
         return reject({err:'order_null'});
      }

      RedisUtil.hset( sails.config.redis.order_redis_hashkey+'_'+userid,orderid,JSON.stringify(order))
      .then(( flag )=>{
            resolve( flag );
      });


   });
}

TradeAssetUtil.getOrderInRedis = function( userid ){
   return new Promise((resolve,reject)=>{
      if(   userid  == null 
         || userid  == undefined 
         || userid  == 'undefined' 
         || userid  == 'null' ){       
         return reject({err:'userid_null'});
      }

      RedisUtil.hvals( sails.config.redis.order_redis_hashkey+'_'+userid)
      .then(( replies )=>{
            resolve( replies );
      });
   });

}

TradeAssetUtil.getTradeAssetHistoryInRedis = function( userid ){
   return new Promise((resolve,reject)=>{
      if(   userid  == null 
         || userid  == undefined 
         || userid  == 'undefined' 
         || userid  == 'null' ){       
         return reject({err:'userid_null'});
      }

      RedisUtil.hkeys( sails.config.redis.trade_asset_history_redis_hashkey+'_'+userid)
      .then(( replies )=>{
            resolve( replies );
      });
   });

}


TradeAssetUtil.getTradeAssetInRedis = function(userid)
{
   return new Promise((resolve,reject)=>{
      
      if(   userid  == null 
         || userid  == undefined 
         || userid  == 'undefined' 
         || userid  == 'null' ){       
         return reject({err:'userid_null'});
      }

      RedisUtil.hget( sails.config.redis.trade_asset_redis_hashkey,userid)
      .then(( asset )=>{    
            resolve( asset );
      });
      
   });
}


TradeAssetUtil.updateTradeAssetInRedis = function(userid,asset)
{
   return new Promise((resolve,reject)=>{
      if(   userid  == null 
         || userid  == undefined 
         || userid  == 'undefined' 
         || userid  == 'null' ){       
         return reject({err:'userid_null'});
      }

      if(   asset  == null 
         || asset  == undefined 
         || asset  == 'undefined' 
         || asset  == 'null' ){   
         return reject({err:'asset_null'});
      }

      RedisUtil.hset( sails.config.redis.trade_asset_redis_hashkey,userid,JSON.stringify(asset))
      .then(( flag )=>{    
            resolve( flag );
      });
   });
}

TradeAssetUtil.updateTradeAssetHistoryInRedis = function(userid,assetHistory,side)
{
   return new Promise((resolve,reject)=>{
      if( userid == null 
         || userid  == undefined 
         || userid  == 'undefined' 
         || userid  == 'null' ){
         reject({err:'userid_null'});
         return ;
      }

      if( assetHistory == null 
         || assetHistory  == undefined 
         || assetHistory  == 'undefined' 
         || assetHistory  == 'null' ){
         reject({err:'assetHistory_null'});
         return ;
      }

       if(   side == null 
         || side  == undefined 
         || side  == 'undefined' 
         || side  == 'null' ){
         reject({err:'side_null'});
         return ;
      }

      var state;
      if( side == sails.config.asset.trade_assets_history_side_deposit ){
         state     = sails.config.asset.trade_assets_history_state_deposited;
      }else if( side == sails.config.asset.trade_assets_history_side_withdraw ){
         state     = sails.config.asset.trade_assets_history_state_withdraw;
      }else if( side == sails.config.asset.trade_assets_history_side_sell ){
         state     = sails.config.asset.trade_assets_history_state_sell;
      }else if( side == sails.config.asset.trade_assets_history_side_buy ){
         state     = sails.config.asset.trade_assets_history_state_buy;
      }



      RedisUtil.hset( sails.config.redis.trade_asset_history_redis_hashkey+'_'+userid,JSON.stringify(assetHistory),state)
      .then(( flag )=>{ 
            resolve( flag );
      });


   });
}


TradeAssetUtil.updateTradeAssetFinishedOrder = function(userid,asset,assetAvailableName,assetFrozenName ){ 
    return new Promise((resolve,reject)=>{

      DBUtil.findTradeAsset(String(userid)).then((record)=>{
         if( record ==undefined || record =='undefined' || record == null || record == 'null' ){
             reject({error:"system_error_userid_notfound"});
         }else {
            if( asset && asset[assetFrozenName] ){
               record[assetFrozenName]    = CommonUtil.subtract(record[assetFrozenName],asset[assetFrozenName]);
            }

            if( asset && asset[assetAvailableName] ){
               record[assetAvailableName] = CommonUtil.add(record[assetAvailableName],asset[assetAvailableName]);
            }

            DBUtil.updateTradeAsset(String(userid),record)
            .then((result)=>{ 
               resolve(result[0]);
            }).catch((err)=>{
                sails.log.error("err",err);
            });
        }
      });
   });
}

TradeAssetUtil.updateTradeAssetPutLimit = function(userid,asset,assetFrozenName,assetAvailableName){
    
    return new Promise((resolve,reject)=>{

      DBUtil.findTradeAsset(String(userid)).then((record)=>{
         if( record ==undefined || record =='undefined' || record == null || record == 'null' ){
             reject({error:"system_error_userid_notfound"});
         }else {
            if( asset && asset[assetFrozenName]){
               record[assetAvailableName] = CommonUtil.subtract(record[assetAvailableName],asset[assetFrozenName]);
               record[assetFrozenName]    = CommonUtil.add(record[assetFrozenName],asset[assetFrozenName]);
            }

            DBUtil.updateTradeAsset(userid,record)
            .then((result)=>{ 
               resolve(result[0]);
            }).catch((err)=>{
                sails.log.error("err",err);
            });
      }})});
}

TradeAssetUtil.updateTradeAssetBalance = function(userid,asset,side,assetAvailableName){
    
    return new Promise((resolve,reject)=>{
      
      DBUtil.findTradeAsset(userid).then((record)=>{
         if( record ==undefined || record =='undefined' ){
            DBUtil.createTradeAsset(asset)
            .then((result)=>{
               resolve(result);
            });
         }else if( side == sails.config.asset.assets_side_withdraw ){
            if( asset && asset[assetAvailableName] ){
               record[assetAvailableName] = CommonUtil.subtract(record[assetAvailableName],asset[assetAvailableName]);
            }

            DBUtil.updateTradeAsset(userid,record)
            .then((result)=>{ 
               resolve(result[0]);
            }).catch((err)=>{
                sails.log.error("err",err);
            });

         }else if( side == sails.config.asset.assets_side_deposit ){
            if( asset && asset[assetAvailableName] ){
               record[assetAvailableName] = CommonUtil.add(record[assetAvailableName],asset[assetAvailableName]);
            }

            
            DBUtil.updateTradeAsset(userid,record)
            .then((result)=>{ 
               resolve(result[0]);
            }).catch((err)=>{
                sails.log.error("err",err);
            });
            
         }
   })
  })
}


TradeAssetUtil.createTradeAssetHistoryBalance = function(userid,asset,amount,detail,side){
	
   if( userid == null 
   	|| userid == undefined 
   	|| userid == 'undefined' 
   	|| userid == 'null' ){
		return {err:'userid_null'};
	}

	if( asset == null 
   	|| asset == undefined 
   	|| asset == 'undefined' 
   	|| asset == 'null' ){
		return {err:'asset_null'};
	}

   if( amount == null 
   	|| amount == undefined 
   	|| amount == 'undefined' 
   	|| amount == 'null' ){
		return {err:'amount_null'};
	}



   var assetHistoryRecord = {};
   
   assetHistoryRecord.userid    = userid;
   assetHistoryRecord.asset     = asset;
   assetHistoryRecord.amount    = parseFloat(amount);
   assetHistoryRecord.timestamp = new Date().getTime();
   assetHistoryRecord.side		  = side;
   assetHistoryRecord.fee       = '0';

   if( side == sails.config.asset.trade_assets_history_side_deposit ){
      assetHistoryRecord.state     = sails.config.asset.trade_assets_history_state_deposited;
   }else if( side == sails.config.asset.trade_assets_history_side_withdraw ){
      assetHistoryRecord.state     = sails.config.asset.trade_assets_history_state_withdraw;
   }
   
  
   if( detail ){
      assetHistoryRecord.detail = detail;
   }
   
   let  promise;

   try{
      promise =  DBUtil.createTradeAssetHistory(assetHistoryRecord);
   }catch(exception){
      sails.log.error('DBUtil.createTradeAssetHistory',assetHistoryRecord);
   }

   return promise;
}


TradeAssetUtil.createTradeAssetHistory = function( assetHistoryRecord ){
   
   let  promise;
   try{
      promise =  DBUtil.createTradeAssetHistory(assetHistoryRecord);
   }catch(exception){
      sails.log.error('DBUtil.createTradeAssetHistory',assetHistoryRecord);
   }
   return promise;
}

module.exports = TradeAssetUtil;


