
let RedisUtil   = require('../utils/RedisUtil');
let CommonUtil  = require('../utils/CommonUtil');
let MQUtil      = require('../utils/MQUtil');
var BatchUtil   = function(){}

MQUtil.create(sails.config.mq.receive_mq_btc)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.receive_mq_btc)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.receive_mq_eth)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.receive_mq_eth)})
.catch((exception)=>{
  sails.log.info(exception);
})
MQUtil.create(sails.config.mq.receive_mq_eos)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.receive_mq_eos)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.receive_mq_elot)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.receive_mq_elot)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.confirm_mq_btc)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.confirm_mq_btc)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.confirm_mq_eth)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.confirm_mq_eth)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.confirm_mq_eos)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.confirm_mq_eos)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.confirm_mq_elot)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.confirm_mq_elot)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.inwallet_mq_btc)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.inwallet_mq_btc)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.inwallet_mq_eth)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.inwallet_mq_eth)})
.catch((exception)=>{
  sails.log.info(exception);
})
MQUtil.create(sails.config.mq.inwallet_mq_eos)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.inwallet_mq_eos)})
.catch((exception)=>{
  sails.log.info(exception);
})
MQUtil.create(sails.config.mq.inwallet_mq_elot)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.inwallet_mq_elot)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.send_mq_btc)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.send_mq_btc)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.send_mq_eth)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.send_mq_eth)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.send_mq_elot)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.send_mq_elot)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.send_mq_eos)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.send_mq_eos)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.order_mq_match)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.order_mq_match)})
.catch((exception)=>{sails.log.info(exception);})


MQUtil.create(sails.config.mq.change_mq_info)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.change_mq_info)})
.catch((exception)=>{sails.log.info(exception);})


MQUtil.create(sails.config.mq.exchange_send_mq_btc)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.send_mq_btc)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.exchange_send_mq_eth)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.send_mq_eth)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.exchange_send_mq_elot)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.send_mq_elot)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.exchange_send_mq_eos)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.send_mq_eos)})
.catch((exception)=>{
  sails.log.info(exception);
})



MQUtil.create(sails.config.mq.exchange_confirm_mq_btc)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.confirm_mq_btc)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.exchange_confirm_mq_eth)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.confirm_mq_eth)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.exchange_confirm_mq_elot)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.confirm_mq_elot)})
.catch((exception)=>{
  sails.log.info(exception);
})

MQUtil.create(sails.config.mq.exchange_confirm_mq_eos)
.then((msg)=>{sails.log.info("MQUtil.create: qname",sails.config.mq.confirm_mq_eos)})
.catch((exception)=>{
  sails.log.info(exception);
})

BatchUtil.markAccountInProcess = function( address ){
    sails.log.info("BatchUtil.markAccountInProcess : account");
    return RedisUtil.hset(sails.config.redis.mq_onreceive_process_check,address,true);
}
BatchUtil.markAccountNotInProcess = function( address ){
    sails.log.info("BatchUtil.markAccountNotInProcess");
    return RedisUtil.hset(sails.config.redis.mq_onreceive_process_check,address,false);
}
BatchUtil.checkAccountProcessState = function( address ){
    sails.log.info("BatchUtil.checkAccountProcessState : address"+ address);
    return RedisUtil.hget(sails.config.redis.mq_onreceive_process_check,address);
}


BatchUtil.putToMQ = function( qname , message){
    sails.log.info("BatchUtil.putToMQ : qname"+ qname + "message" +message);
    return MQUtil.sendMessage(qname,message);
}

BatchUtil.getFromMQ = function( qname,vt ){
    //sails.log.debug("BatchUtil.getFromMQ : qname "+ qname );
    let resp;
    try
    {
    	resp =MQUtil.receiveMessage(qname,vt);
    }
    catch(exception)
    {
    	sails.log.error("BatchUtil.getFromMQ : qname "+ qname );
    	sails.log.error(exception);

    }
    return resp;
}

BatchUtil.removeFromMQ = function( qname,id ){
    sails.log.debug("BatchUtil.removeFromMQ : qname"+ qname +" id: "+id );
    return MQUtil.deleteMessage(qname,id);
}




module.exports = BatchUtil