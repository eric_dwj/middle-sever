var BTCUtil        = function(){}
const btcapi       = require('bitcoinjs-lib');
let CommonUtil     = require('../utils/CommonUtil');
let JsonRPCUtil    = require('../utils/JsonRPCUtil');
let bitcore        = sails.config.globals.bitcore;
let TradeAssetUtil = require('../utils/TradeAssetUtil');
let currentnet;
if( sails.config.globals.btc_current_net==0 ){
    currentnet= btcapi.networks.testnet;  
}else if( sails.config.globals.btc_current_net==1 ){
    currentnet = btcapi.networks.bitcoin;
}

BTCUtil.txConfirmSchedule = function(){
    CommonUtil.scheduler(sails.config.asset.utxo_confirm_cron,async ()=>{
        var txs = await TradeAssetUtil.loadAssetUTXO(sails.config.asset.assets_btc_name);
        for ( var i = txs.length - 1; i >= 0; i-- ) {
          var tx = await BTCUtil.gettransaction( txs[i].tx );
          if( tx && txs[i].side == sails.config.asset.assets_side_withdraw && tx.confirmations >= 1 ){
            TradeAssetUtil.updateAssetTx( txs[i].tx , sails.config.asset.tx_confirmed );
          }
        }     
    });
}

BTCUtil.publicKeyFromPK = function(pk){
    let keypair;
    try{
         keypair = btcapi.ECPair.fromWIF(pk,currentnet);
    }catch(exception){
         sails.log.error(exception);
    }
    return keypair.publicKey.toString("hex");
}

BTCUtil.addressFromPublicKey = function(publicKey){
    return btcapi.payments.p2pkh({ pubkey: Buffer.from(publicKey, "hex"),network:currentnet }).address
}

BTCUtil.addressFromPK = function(pk){
    return BTCUtil.addressFromPublicKey(BTCUtil.publicKeyFromPK(pk));
}

BTCUtil.createRawTransaction = function(unspent,toAddress,amountToSend,pk,fromAddress,transactionFee){
console.log('unspent',unspent);
return new Promise((resolve, reject) => {
  if( ! unspent ){
     reject("unspent_not_existed");
   }

  if( toAddress  == null || toAddress ==undefined ){
     reject("toAddress_not_existed");
   }

  if( amountToSend == null || amountToSend ==undefined ){
     reject("amountToSend_not_existed");
   }

  if( pk == null || pk ==undefined ){
     reject("pk_not_existed");
  } 


  if( fromAddress == null || fromAddress == undefined  ){
     reject("fromAddress_not_existed");
  } 

  if(  transactionFee== null || transactionFee == undefined  ){
     reject("transactionFee_not_existed");
  }   
  
  try{
      console.log('#####currentnet#####',currentnet);
      const tx = new btcapi.TransactionBuilder(currentnet);
      var accountNumber =0;
      var amountWeHave = 0;
      while ( accountNumber < unspent.length) {

           amountWeHave = amountWeHave +  unspent[accountNumber].satoshis ;
           console.log('[unspent[accountNumber].txid] :'+unspent[accountNumber].txid + 'unspent[accountNumber].vout :'+unspent[accountNumber].vout);
           tx.addInput( unspent[accountNumber].txid , parseInt( unspent[accountNumber].vout ) );
            
           if( amountWeHave >= amountToSend + transactionFee ){
             break;
           };

           accountNumber++;
      }

      console.log("###########amountWeHave:#######",amountWeHave);
      console.log("###########transactionFee:#######",transactionFee);
      var amountToKeep = amountWeHave-amountToSend-transactionFee;

      console.log("######a########,toAddress",toAddress);
      console.log("######a########,amountToSend",amountToSend);
       
      tx.addOutput(toAddress, parseInt(amountToSend));
       
      console.log("######bbbb########,fromAddress",fromAddress);
      console.log("######bbbb########,amountToKeep",amountToKeep);

      tx.addOutput(fromAddress, parseInt(amountToKeep));
      
      console.log("######a########,pk",pk);
      for (var i = 0; i <= accountNumber; i++) {
           tx.sign(i, btcapi.ECPair.fromWIF(pk,currentnet));
      }
       
      var rawTransactionData = tx.build().toHex();
      console.log("rawTransactionData",rawTransactionData);
      resolve(rawTransactionData);

  }catch(exception){
    sails.log.error(exception);
    reject(exception);
  }
    
});
}


BTCUtil.getbalance = function(address){ 
    return new Promise((resolve, reject) => { 
       console.log(bitcore.url + bitcore.get_address_balance + address);
        JsonRPCUtil.Get(bitcore.url + bitcore.get_address_balance + address)
        .then(( resp )=>{
            console.log('[resp]: ',resp);
            let data = JSON.parse(resp);
            try{
                if( data.balance ){
                  resolve( data.balance );
                }else{
                  reject({err:'get_address_balance_error'});
                }
            }catch(exception){
              sails.log.error(exception);
              reject(exception);
            }
        })
    });
}
 
BTCUtil.gettransaction = function(txid){
    return new Promise((resolve, reject) => {
        JsonRPCUtil.Get(bitcore.url+ bitcore.get_tx+txid)
        .then((resp)=>{
          let data  = JSON.parse( resp );
          try{ 
              if( data.txid ){
                  resolve(data);
              }else{
                  reject({err:'get_tx_error'});
              }
          }catch(exception){
              sails.log.error(exception);
          }
        })
    });
}

BTCUtil.sendrawtransaction = function(rawTransactionData){
    return new Promise((resolve, reject) => {
        var postdata= { rawtx : rawTransactionData }; 
        JsonRPCUtil.PostV2( postdata,bitcore.url + bitcore.send_tx )
        .then((resp)=>{
           
            if( resp.txid ){
                resolve( resp.txid );
            }else{
                reject({err:'send_tx_error'});
            }
        })
    });
} 

BTCUtil.getrawtransaction = function(txid){
    return BTCUtil.gettransaction(txid);

} 

BTCUtil.listunspent = function(address){
   console.log('[BTCUtil.listunspent]',address);
    return new Promise((resolve, reject) => {
        JsonRPCUtil.Get( bitcore.url + bitcore.is_address_valid + address + bitcore.get_tx_unspent )
        .then((resp)=>{
          let data = JSON.parse( resp );
          try{
               if( data && data.length > 0 ){
                  resolve( data );
                }else{
                  reject({err:'get_tx_unspent_error'});
                }
          }catch(exception){
              reject(exception);
          }
        })
     });
}

BTCUtil.getCurrentBlock = function(){
    return new Promise((resolve, reject) => {
        JsonRPCUtil.Get( bitcore.url+ bitcore.get_info )
        .then((resp)=>{
          let data = JSON.parse( resp );
          try{
                if( data && data.info ){
                  resolve( data.info.blocks );
                }else{
                  reject({err:'get_blocks_error'});
                }
          }catch(exception){
              resolve(0);
          } 
        })
     });
} 

BTCUtil.validateaddress = function(address){
    return new Promise((resolve, reject) => {
        JsonRPCUtil.Get( bitcore.url + bitcore.is_address_valid + address )
        .then((resp)=>{
          let data = JSON.parse( resp );
            try{
                  if( data && data.addrStr){
                    resolve(true);
                  }else{
                    resolve(false);
                  }
            }catch(exception){
                sails.log.error(exception);
                reject(exception);
            }
        })
     });
}

BTCUtil.transfer = function(toAddress,amountToSend,pk,transactionFee){
          return new Promise((resolve, reject) => {
            let fromAddress   = BTCUtil.addressFromPublicKey(BTCUtil.publicKeyFromPK(pk));
            BTCUtil.listunspent(fromAddress).then((unspent)=>{
                      BTCUtil.createRawTransaction(
                        unspent,
                        toAddress,
                        parseInt( CommonUtil.multiply(amountToSend,100000000)),
                        pk,
                        fromAddress,
                        parseInt( CommonUtil.multiply(transactionFee,100000000))
                        )
                      .then((rawTransactionHex)=>{
                            BTCUtil.sendrawtransaction(rawTransactionHex)
                            .then((txid)=>{
                                resolve(txid)
                            }).catch((sendrawtransactionException)=>{reject(sendrawtransactionException)});
                      })
                      .catch((createRawTransactionException)=>{reject(createRawTransactionException)});
            }).catch((listunspentException)=>{reject(listunspentException)});
          });
} 

module.exports = BTCUtil