module.exports = {
       eth: function(address) {
          return ETHService.balance(address);
        },
       btc: function(address){
        return BTCService.balance(address);
       },
       ltc: function(address){
        return LTCService.balance(address);
       },
       token:function(address,tokenName){
        return TokenService.balance(address,tokenName);
       }
}