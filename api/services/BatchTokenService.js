/**
 * 处理消息队列任务任务，全部作为callback注入BatchReceiveService
 */
var TokenBlockComfirmation             = require('../classes/TokenBlockComfirmation.js');
var TokenBatchReceiveByAccount         = require('../classes/TokenBatchReceiveByAccount.js');
var TokenBatchReceiveByInWallet        = require('../classes/TokenBatchReceiveByInWallet.js');
var TokenSendConfirmBatch              = require('../classes/TokenSendConfirmBatch.js');
var ExchangeTokenBlockConfirmation     = require('../classes/ExchangeTokenBlockConfirmation.js');
var ExchangeTokenSendConfirmBatch      = require('../classes/ExchangeTokenSendConfirmBatch.js');
const  exchangeTokenSendConfirmBatch   = new ExchangeTokenSendConfirmBatch();
const  exchangeTokenBlockConfirmation  = new ExchangeTokenBlockConfirmation();
const  tokenSendConfirmBatch           = new TokenSendConfirmBatch();
const  tokenBatchReceiveByInWallet     = new TokenBatchReceiveByInWallet();
const  tokenBlockComfirmation          = new TokenBlockComfirmation();
const  tokenBatchReceiveByAccount      = new TokenBatchReceiveByAccount();
module.exports = {
        processReceiveByAccount: function(){ 
            BatchReceiveService.processReceiveByAccount( tokenBatchReceiveByAccount );                 
        },
        processRecevieByInWallet:function(){
            BatchReceiveService.processRecevieByInWallet( tokenBatchReceiveByInWallet );
        },
        processBlockComfirmation:function(){
            BatchReceiveService.processBlockComfirmation( tokenBlockComfirmation );
        },
        processSend:function(){
        	BatchSendService.processSend( tokenSendConfirmBatch );
        },
        processExchangeReceive:function(){
            ExchangeBatchReceiveService.processReceiveByAccount( exchangeTokenBlockConfirmation );
        },
        processExchangeSend:function(){
           ExchangeBatchSendService.processExchangeSend( exchangeTokenSendConfirmBatch );
        }
}