/**
 * 资产管理服务，主要用于充币，取币以及交易所余额管理
 */
let CommonUtil   = require('../utils/CommonUtil');
let AssetUtil    = require('../utils/AssetUtil');
let ETHUtil      = require('../utils/ETHUtil');

module.exports = {
       
       //轮询eth在redis里面数据，完成充值
       createAsset : function( userid ) {
          return AssetUtil.createAsset( userid );
       }


}