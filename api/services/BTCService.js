/**
 * 封装的RPC调用bitcoind的api
   调用BTCUtil一起完成BTC的账号生成和转账，查询等任务
 */

let BTCUtil = require('../utils/BTCUtil');

module.exports = {
       
   
      transferWithBalanceCheck:function(toAddress,amountToSend,pk,transactionFee){

         return new Promise((resolve, reject) => {
         console.log('toAddress :'+toAddress+' amountToSend : '+amountToSend + ' pk : '+pk +' transactionFee: '+transactionFee);
          BTCService.balance( BTCUtil.addressFromPK(pk)).then((balance)=>{
           
            if( amountToSend > balance ){
             console.log('balance_not_enough :'+ balance);
                 reject({error:"balance_not_enough"});
            }else{      
              BTCUtil.transfer(toAddress,amountToSend,pk,transactionFee)
              .then((result)=>{
                  resolve(result);
              }).catch((exception)=>{
                  reject(exception);
              });
            }
          });
        });
       },

       //获取余额
      balance: function(address){
        console.log('address',address);
        return BTCUtil.getbalance(address);
     
       },

      getTransaction: function(txid) {
         sails.log.info("BTCService.getrawtransaction");
        return BTCUtil.getrawtransaction(txid);
       },

      getCurrentBlock:function(){
        return BTCUtil.getCurrentBlock();
      },

      validateAddress:function(address){
        return BTCUtil.validateaddress(address);
      },

      startConfirmSchedule:function(){
        BTCUtil.txConfirmSchedule();
      }

}




