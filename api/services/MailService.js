/**
 * EmailGerneralServiceController
 *  通用邮件服务，通过邮件模版（html模版），可以置换四个参数，并且把邮件发送出去
 *  这个API和EmailGeneralServiceController一起结合使用
 */

var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var transporter = nodemailer.createTransport(smtpTransport({

host: 'smtp.doxdox.conoha.io',
secure:true,
port:465,
auth: {
user: "admin@doxdox.conoha.io",
pass: "Zaq12wsx@@"
}
    ,tls: {
        // do not fail on invalid certs
        rejectUnauthorized: false
    }

}));



module.exports = {

       sendMail: function(mailOptions) {
        　return new Promise((resolve, reject) => {
          transporter.sendMail(mailOptions, function(error, info){
              if (error) {
                reject(error);
              } else {
                resolve(info);
              }
          });
        });
       },
        generateMailHtml: function(metadataCode,code1,code2,code3,code4) {
        　return new Promise((resolve, reject) => {
          
             GeneralMetaData.findOne({code:metadataCode})
             .exec(function(err, record) {
              if(err || (!record))
              {
                reject({error:"GeneralMetaData_Table_Code_"+metadataCode+"_htmlTemplate_Not_existed"})
              }
              else
              {
                //var htmlTemplate='<h3>您的验证码为：##value1## </h3>'
                 var html="";

                  if(code1)
                  html = record.description.replace("##value1##",code1);
                  if(code2)
                  html = html.replace("##value2##",code2)
                  if(code3)
                  html = html.replace("##value3##",code3)
                  if(code4)
                  html = html.replace("##value4##",code4);
                  resolve(html);
              }


              });
    
           
        });
       }

}