/**
 * 封装的Web3调用的api
   调用ETHUtil一起完成ETH的账号生成和转账，查询等任务
 */

let ETHUtil   = require('../utils/ETHUtil');


module.exports = {
      transferWithBalanceCheck: function(to,pk,gasPrice,amount) {
         return new Promise((resolve,reject)=>{
           ETHUtil.getBalance( ETHUtil.getAddressFromPk(pk))
           .then((balance)=>{
              if(balance<amount){
                    reject({error:"balance_not_enough",balance:balance,amount:amount});
              }else{
                ETHUtil.transfer(ETHUtil.getAddressFromPk(pk),to,pk,gasPrice,amount)
                .then((txid)=>{
                    resolve(txid);
                }).catch((exception)=>{
                    reject(exception);
                });
              }
           });
         }); 
      },
      balance: function(address){
        var result;
        try{
          result =  ETHUtil.getBalance(address);
        }catch(exception){
          sails.log.error("ETHService.balance",address);
          sails.log.error(exception);
        }
        return result;
      },
      getTransaction: function(txid) {
        return ;
      },
      getCurrentBlock:function(){
        return ETHUtil.getCurrentBlock();
      },
      validateAddress:function(address){
        return ETHUtil.validateaddress(address);
      },
      ranTransaction: function(rawTransactionData) {
        return ETHUtil.sendTransaction(rawTransactionData);    
      },
      startConfirmSchedule:function(){
        ETHUtil.txConfirmSchedule();
      }

}