let CommonUtil = require('../utils/CommonUtil');

module.exports = {
       token:function(to,pk,gasPrice,amount,tokenName){
          var promise;
          try{
              promise = TokenService.transferWithBalanceCheck(to,pk,gasPrice,amount,tokenName);
          }catch(exceptions){
            sails.log.error("token: function(to,pk,gasPrice,amount,tokenName)  to: "+to +" gasPrice: "+gasPrice +" amount: " +amount + " tokenName: "+ tokenName);
            sails.log.error(exceptions);
          }
          return promise;
       },
       eth: function(to,pk,gasPrice,amount) {
          var promise;
          try{
              promise = ETHService.transferWithBalanceCheck(to,pk,gasPrice,amount);
          }catch(exceptions){
            sails.log.error("eth: function(to,pk,gasPrice,amount)  to: "+to +" gasPrice: "+gasPrice +" amount: " +amount)
            sails.log.error(exceptions);
          }
          return promise;
       },
       btc: function(to,amountToSend,pk,transactionFee){
          var promise;
          try{
              promise = BTCService.transferWithBalanceCheck(to,amountToSend,pk,transactionFee); 
          }catch(exceptions){
            sails.log.error("btc: function(to,amountToSend,pk,transactionFee)  to: "+to +" amountToSend: "+amountToSend +" pk: " +pk +" transactionFee: " +transactionFee);
            sails.log.error(exceptions);
          }
          return promise;
       },
       ltc: function(to,amountToSend,pk,transactionFee){
            var promise;
            try{
                promise = LTCService.transferWithBalanceCheck(to,amountToSend,pk,transactionFee); 
            }catch(exceptions){
            sails.log.error("btc: function(to,amountToSend,pk,transactionFee)  to: "+to +" amountToSend: "+amountToSend +" pk: " +pk +" transactionFee: " +transactionFee);
            sails.log.error(exceptions);
            }
            return promise;
       },      
       qrcode:function(data){
          return CommonUtil.qrcode(data);
       },
       getTicker:function(){
           return CommonUtil.getTicker();
       }
}