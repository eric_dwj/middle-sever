
let CommonUtil                                = require('../utils/CommonUtil');
let ExchangeSendConfirmProcessor = require('../classes/ExchangeSendConfirmProcessor');
module.exports = {
    processExchangeSend:async function(batch){
      while( true ){
        var resp = await batch.getSendObjFromConfrimMQ()
        if( resp && resp.message ){
           let sendObject = JSON.parse(resp.message);
           try{
              var processor = new ExchangeSendConfirmProcessor(sendObject,resp.id,batch)
              await processor.execute();
           }catch(exception){
              sails.log.error("ExchangeBatchSendService.execute",exception);
           }
        }else{
          await CommonUtil.sleep(sails.config.mq.no_data_sleep);
        }  
      }
    }
}