/**
 * 像交易所存钱的批次任务，通过轮询确认充值信息
 *
 */
let BatchUtil  = require('../utils/BatchUtil');
let DBUtil       = require('../utils/DBUtil');
let CommonUtil   = require('../utils/CommonUtil');



module.exports = {
      //注册的时候，将支付账号信息保存到redis里面
      addReceiveObjectToReceiveMQ:async function(receiveObject){
        
        let assetsName        = 'assets_' + String( receiveObject.assetname ).toLowerCase() + '_name';
        var assetAccountName  = String( receiveObject.assetname ).toUpperCase() + 'Account';
        let receiveMQToken    = 'receive_mq_' + String( receiveObject.assetname ).toLowerCase();

        if( receiveObject.assetname == sails.config.asset.assets_eth_name)
        {
          let check =await BatchUtil.checkAccountProcessState(receiveObject.account.ETHAccount.address);
          if( check != true || check != "true" )
          {
              BatchUtil.markAccountInProcess(receiveObject.account.ETHAccount.address);
              BatchUtil.putToMQ(sails.config.mq.receive_mq_eth, JSON.stringify(receiveObject))
              .then((result)=>{
                    sails.log.debug("########result:########",result);
              });
          }
        }else if( receiveObject.assetname == sails.config.asset.assets_btc_name)
        {
          let check =await BatchUtil.checkAccountProcessState(receiveObject.account.BTCAccount.address);
          if( check != true || check != 'true' )
          {
              BatchUtil.markAccountInProcess(receiveObject.account.BTCAccount.address);
              BatchUtil.putToMQ(sails.config.mq.receive_mq_btc, JSON.stringify(receiveObject))
              .then((result)=>{
                    sails.log.info("########result:########",result);
              });
          }
        }else if( sails.config.asset[assetsName] )
        {
          let check =await BatchUtil.checkAccountProcessState(receiveObject.account[assetAccountName].address);
          if( check != true || check != 'true' )
          {
              BatchUtil.markAccountInProcess(receiveObject.account[assetAccountName].address);
              BatchUtil.putToMQ(sails.config.mq[receiveMQToken], JSON.stringify(receiveObject))
              .then((result)=>{
                    sails.log.info("########result:########",result);
              });
          }
        }


      },

      run:function(){
         
         ReceiveService.runBTC();
         ReceiveService.runETH();
         ReceiveService.runToken();

      },
      runBTC:function(){
       BatchBTCService.processRecevieByInWallet();
       BatchBTCService.processReceiveByAccount();
       BatchBTCService.processBlockComfirmation();

      },
      runETH:function(){
        BatchETHService.processRecevieByInWallet();
        BatchETHService.processReceiveByAccount();
        BatchETHService.processBlockComfirmation();
      },

      runToken:function(){
       BatchTokenService.processRecevieByInWallet();
       BatchTokenService.processReceiveByAccount();
       BatchTokenService.processBlockComfirmation();
      }
}