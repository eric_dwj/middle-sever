/**
 * 封装了轮询任务,采用BatchReceiveService模版方法等等设计模式
 */

let CommonUtil = require('../utils/CommonUtil');
let ReceiveByAccountProcesser         = require('../classes/ReceiveByAccountProcesser');
let ReceiveBlockComfirmationProcessor = require('../classes/ReceiveBlockComfirmationProcessor');
let ReceiveByInWalletProcessor        = require('../classes/ReceiveByInWalletProcessor');

module.exports = {


    processReceiveByAccount: function(batch){
     
      batch.getReceiveObjectFromReceiveMQ()
      .then( async (resp)=>{

       
       if( resp && resp.message )
       {

           let receiveObject = JSON.parse(resp.message);
           
           try
           {
              var processor = new ReceiveByAccountProcesser(receiveObject,resp.id,batch)
              await processor.execute();
           }catch(exception)
           {
              sails.log.error("ReceiveByAccountProcesser.execute",exception);
           }

       }
       else
       {
          await CommonUtil.sleep(sails.config.mq.no_data_sleep);
          // sails.log.debug("processReceiveByAccount sleeping");
       } 
        BatchReceiveService.processReceiveByAccount(batch);
      })
    },

    processBlockComfirmation:async function(batch)
    {
        batch.getReceiveObjFromConfrimMQ()
        .then(async (resp)=>{
          //console.log("###aaa####")

        if( resp && resp.message )
        {
           let receiveObject = JSON.parse(resp.message);
           
           try
           {
             var processor = new ReceiveBlockComfirmationProcessor(receiveObject,resp.id,batch);
             await processor.execute();
           }
           catch(exception)
           {
              sails.log.error('ReceiveBlockComfirmationProcessor.execute',exception);
           }
          

        }
        else
        {
            await CommonUtil.sleep(sails.config.mq.no_data_sleep);
            // sails.log.debug("processBlockComfirmation sleeping");
        } 

          BatchReceiveService.processBlockComfirmation(batch);
        })
    },

    processRecevieByInWallet:async function(batch) 
    {
       batch.getReceiveObjectFromInWalletMQ()
        .then(async (resp)=>{
          //console.log("###aaa####")

        if( resp && resp.message )
        {
           let receiveObject = JSON.parse(resp.message);
           
           try
           {
             var processor = new ReceiveByInWalletProcessor(receiveObject,resp.id,batch);
             await processor.execute();
           }
           catch(exception)
           {
              sails.log.error('ReceiveByInWalletProcessor.execute',exception);
           }
          

        }
        else
         {
            await CommonUtil.sleep(sails.config.mq.no_data_sleep);
            // sails.log.debug("processRecevieByInWallet sleeping");
         } 

          BatchReceiveService.processRecevieByInWallet(batch);
        })
      
    }
    

       

}