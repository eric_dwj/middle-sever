let Util                = require('../utils/JsonRPCUtil');
let TradeAssetUtil      = require('../utils/TradeAssetUtil');
const Trade             = require('../classes/Trade');
let TradeMatchProcessor = require('../classes/TradeMatchProcessor');
let tradingEx           = new Trade();
module.exports = {
    fetchAssetTx:function(userid){
        return TradeAssetUtil.loadAssetTx(userid);
    },
    matchProcess:function(){
        var processor = new TradeMatchProcessor();
        processor.execute();
    },
    orderBook:function(  market , side , offset , limit ){
        return tradingEx.orderBook( market , side , offset , limit );
    },
    getOrder:function( userid ){
        return tradingEx.getOrder( userid );  
    },
    getAssetHistory( userid ){
        return tradingEx.getAssetHistory( userid ); 
    },
    updateBalance: function( userid,asset,business,amount,detail){
        return tradingEx.updateBalance( userid, asset, business, amount,detail );  
    },
    getBalance:function( userid ){
        return tradingEx.getBalance( userid );
    },
    putLimit:function( userid , market , side , amount , price  ){
        return tradingEx.pubLimit( userid,market,side,amount,price );
    },
    cancelOrder:function( userid,market,orderid){
        return tradingEx.cancelOrder( userid,market,orderid);
    },
    apiCall: function(userid, body) {
        return new Promise(function (resolve, reject) {
            var data = { 
                id: userid,
                method: body.method,
                params: body.params
            };

            Util.Post(data).then((resData)=>{
                resolve(resData);
            });
        })
    },
    JsonRPCCall: function(data) {
        return new Promise(function (resolve, reject) {
            Util.Post(data).then((resData)=>{
                resolve(resData);
            });
        })
    }
}