/**
 * 用户注册等相关服务
 */
let md5        = require('md5');
let CommonUtil = require('../utils/CommonUtil');
let UserUtil   = require('../utils/UserUtil');

module.exports = {
    encrypt :function( data , password ){
      return CommonUtil.encrypt( data , password );
    },
    decrypt:function( data , password ){
      return CommonUtil.decrypt( data , password );

    },
    //用户注册
    getAccountByUserid : function(userid){

        return new Promise(function (resolve, reject) {

            Users.find({userid:userid})
            .exec(function (err, user) {
              if (err) {
                reject(err);       
              }
              if (user && user.length>0 ) {
                resolve(user[0].account);
              }
            });

        });
    },

    find:function(mnemonic){
      return new Promise((resolve,reject)=>{

        var encryptedMnemonic = md5(mnemonic);

        Users.findOne({encryptedMnemonic:encryptedMnemonic})
        .exec(function (err, user) {
            if (err) {
              reject(err);       
            }
            if (user) {
              
              UserUtil.processInnerAccount(user.account);
              delete user.encryptedMnemonic;
              delete user.id;

              resolve(user);
            }else{
                resolve(null);
            }
        });
      })
      

    },

    register: function(userobj) {
        return new Promise(function (resolve, reject) {
        
            userobj.userid = UserUtil.generateUserID();

            //产生内部充值账号
            userobj.account = AccountService.generateAccount();

            //产生用户钱包
            let wallet;
            if (userobj.mnemonic) {
                wallet = AccountService.impoartAccountByMnemonic( userobj.mnemonic )
            }else{
                wallet = AccountService.generateAccount();
            }
            userobj.encryptedMnemonic = md5(wallet.mnemonic);


            //产生资产列表
            AssetService.createAsset( userobj.userid );
           
            userobj.timestamp = new Date().getTime();
    
            Users.findOrCreate({userid:userobj.userid},userobj )
            .exec(function (err, user) {
              if (err) {
                reject(err);
              }
              if (user) {
                UserUtil.processInnerAccount(user.account);
                user.wallet = wallet;
                resolve(user);
              }else{
                  resolve(null);
              }
            });
        })
    }
}