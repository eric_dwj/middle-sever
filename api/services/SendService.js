
let BatchUtil    = require('../utils/BatchUtil');
let DBUtil       = require('../utils/DBUtil');
let CommonUtil   = require('../utils/CommonUtil');

module.exports = {
     
      addSendObjectToSendMQ:function(sendObject){
      // sendObject={timestamp:,userid:,assetname:,address:,size:} 
      return new Promise( async (resolve,reject)=>{


        if( !sendObject.timestamp )
        {
            return reject('timestamp_not_existed');
        }

        if( !sendObject.userid )
        {
            return reject('userid_not_existed');
        }

        if( !sendObject.assetname )
        {
            return reject('assetname_not_existed');
        }

        if( !sendObject.address )
        {
            return reject('address_not_existed');
        }

        if( !sendObject.size )
        {
            return reject('size_not_existed');
        }

        let assetsName         = 'assets_' + String( sendObject.assetname ).toLowerCase() + '_name';
        let tokenSendThreshold = 'token_'  + String(sendObject.assetname).toLowerCase()+'_send_threshold';
        let exchangSendMQToken = 'send_mq_'+ String(sendObject.assetname).toLowerCase();

        if( sendObject.assetname == sails.config.asset.assets_eth_name)
        {
            if( sendObject.size < sails.config.globals.eth_send_threshold )
            {
                return reject('size_not_enough');
            }
            
            BatchUtil.putToMQ(sails.config.mq.send_mq_eth, JSON.stringify(sendObject))
            .then((result)=>{
                  return resolve(result);
            });
        }
        else if( sendObject.assetname == sails.config.asset.assets_btc_name)
        {
            if( sendObject.size < sails.config.globals.btc_send_threshold )
            {
                return reject('size_not_enough');
            }

            BatchUtil.putToMQ(sails.config.mq.send_mq_btc, JSON.stringify(sendObject))
            .then((result)=>{
                return resolve(result);
            });
        }
         else if( sails.config.asset[assetsName])
        {

            if( sendObject.size < sails.config.globals[tokenSendThreshold] )
            {
                return reject('size_not_enough');
            }

            BatchUtil.putToMQ(sails.config.mq[exchangSendMQToken], JSON.stringify(sendObject))
            .then((result)=>{
                return resolve(result);
            });
        }

      });
      },
      run:function(){ 
        SendService.runBTC();
        SendService.runETH();
        SendService.runToken();
      },
      runBTC:function(){
        BatchBTCService.processSend();
      },
      runETH:function(){
        BatchETHService.processSend();
      },
      runToken:function(){
        BatchTokenService.processSend();
      }
     
      
     
   


}