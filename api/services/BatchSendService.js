
let CommonUtil               = require('../utils/CommonUtil');
let SendConfirmProcessor     = require('../classes/SendConfirmProcessor');

module.exports = {
    //处理出账的框架
     processSend:function(batch){
      
      batch.getSendObjFromConfrimMQ()
      .then( async (resp)=>{
       if( resp && resp.message )
       {
           let sendObject = JSON.parse(resp.message);
           try
           {
              var processor = new SendConfirmProcessor(sendObject,resp.id,batch)
              await processor.execute();
           }catch(exception)
           {
              sails.log.error("SendConfirmProcessor.execute",exception);
           }
       }
       else
       {
        
          await CommonUtil.sleep(sails.config.mq.no_data_sleep);
       } 
        BatchSendService.processSend(batch);
      })
    }
}