/**
 * 处理消息队列任务任务，全部作为callback注入BatchReceiveService
 */
var BTCBlockComfirmation           = require('../classes/BTCBlockComfirmation.js');
var BTCBatchReceiveByAccount       = require('../classes/BTCBatchReceiveByAccount.js');
var BTCBatchReceiveByInWallet      = require('../classes/BTCBatchReceiveByInWallet.js');
var BTCSendConfirmBatch            = require('../classes/BTCSendConfirmBatch.js');
var ExchangeBTCBlockConfirmation   = require('../classes/ExchangeBTCBlockConfirmation.js');
var ExchangeBTCSendConfirmBatch   = require('../classes/ExchangeBTCSendConfirmBatch.js');
const  exchangeBTCSendConfirmBatch = new ExchangeBTCSendConfirmBatch();
const  exchangeBTCBlockConfirmation = new ExchangeBTCBlockConfirmation();
const  btcSendConfirmBatch         = new BTCSendConfirmBatch();
const  btcBatchReceiveByInWallet   = new BTCBatchReceiveByInWallet();
const  btcBlockComfirmation        = new BTCBlockComfirmation();
const  btcBatchReceiveByAccount    = new BTCBatchReceiveByAccount();
module.exports = {
        processReceiveByAccount: function(){ 
            BatchReceiveService.processReceiveByAccount( btcBatchReceiveByAccount );                 
        },
        processRecevieByInWallet:function(){
            BatchReceiveService.processRecevieByInWallet( btcBatchReceiveByInWallet );
        },
        processBlockComfirmation:function(){
            BatchReceiveService.processBlockComfirmation( btcBlockComfirmation );
        },
        processSend:function(){
        	BatchSendService.processSend( btcSendConfirmBatch );
        },
        processExchangeReceive:function(){
           ExchangeBatchReceiveService.processReceiveByAccount( exchangeBTCBlockConfirmation );
        },
        processExchangeSend:function(){
           ExchangeBatchSendService.processExchangeSend( exchangeBTCSendConfirmBatch );
        }
}