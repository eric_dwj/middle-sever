let BatchUtil      = require('../utils/BatchUtil');
let DBUtil         = require('../utils/DBUtil');
let CommonUtil     = require('../utils/CommonUtil');
let TradeAssetUtil = require('../utils/TradeAssetUtil');
//TradeAssetUtil.getTradeAssetInRedis
module.exports = {
     
      addSendObjectToSendMQ:function(sendObject){
      // sendObject={timestamp:,userid:,assetname:,address:,size:} 
      return new Promise( async (resolve,reject)=>{
        let assetsName         = 'assets_' + String( sendObject.assetname ).toLowerCase() + '_name';
        let tokenSendThreshold = 'token_'+String(sendObject.assetname).toLowerCase()+'_send_threshold';
        let assetAvailableName = String(sendObject.assetname).toLowerCase()+'Available';
        let exchangSendMQToken = 'exchange_send_mq_'+String(sendObject.assetname).toLowerCase();


        
        if( !sendObject.timestamp )
        {
            return reject('timestamp_not_existed');
        }

        if( !sendObject.userid )
        {
            return reject('userid_not_existed');
        }

        if( !sendObject.assetname )
        {
            return reject('assetname_not_existed');
        }

        if( !sendObject.address )
        {
            return reject('address_not_existed');
        }

        if( !sendObject.detail )
        {
            return reject('detail_not_existed');
        }

        if( !sendObject.size )
        {
            return reject('size_not_existed');
        }

        let tradeAsset = JSON.parse(await TradeAssetUtil.getTradeAssetInRedis(sendObject.userid) );



        if( sendObject.assetname == sails.config.asset.assets_eth_name)
        {


            if( parseFloat( sendObject.size )  <= sails.config.globals.eth_send_threshold )
            {
                return reject('size_not_enough');
            }else if( parseFloat( tradeAsset.ethAvailable ) < parseFloat( sendObject.size ) )
            {
                return reject('eth_balance_not_enough');
            }else
            {
                 BatchUtil.putToMQ(sails.config.mq.exchange_send_mq_eth, JSON.stringify(sendObject))
                    .then((result)=>{
                          return resolve(result);
                    });

            }
            
           
        }
        else if( sendObject.assetname == sails.config.asset.assets_btc_name)
        {


            if( parseFloat( sendObject.size ) <= sails.config.globals.btc_send_threshold )
            {
                return reject('size_not_enough');
            }else if( parseFloat( tradeAsset.btcAvailable ) < parseFloat( sendObject.size ))
            {
                return reject('btc_balance_not_enough');
            }else 
            {
                BatchUtil.putToMQ(sails.config.mq.exchange_send_mq_btc, JSON.stringify(sendObject))
                .then((result)=>{
                    return resolve(result);
                });
            }

           
        }
         else if( sails.config.asset[assetsName] )
        {
            if( parseFloat( sendObject.size )<= sails.config.globals[tokenSendThreshold] )
            {
                return reject('size_not_enough');
            }else if( parseFloat( tradeAsset[assetAvailableName] ) < parseFloat( sendObject.size )  )
            {
                return reject('btc_balance_not_enough');
            }
            else
            {
                BatchUtil.putToMQ(sails.config.mq[exchangSendMQToken], JSON.stringify(sendObject))
                .then((result)=>{
                    return resolve(result);
                });
            }
        }

      });
      },
      run:function(){ 
        ExchangeSendService.runBTC();
        ExchangeSendService.runETH();
        ExchangeSendService.runToken();
      },
      runBTC:function(){
        BatchBTCService.processExchangeSend();
      },
      runETH:function(){
        BatchETHService.processExchangeSend();
      },
      runToken:function(){
        BatchTokenService.processExchangeSend();
      }
     
      
     
   


}