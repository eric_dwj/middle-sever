/**
 * 封装的Web3调用的api
   调用ETHUtil一起完成ETH的账号生成和转账，查询等任务
 */

let ETHUtil     = require('../utils/ETHUtil');
let TokenUtil   = require('../utils/TokenUtil');
let CommonUtil   = require('../utils/CommonUtil');

module.exports = {

       transferWithBalanceCheck: function(to,pk,gasPrice,amount,tokenName) {
         return new Promise((resolve,reject)=>{

           let contractAddress;
           let amountWithDecimalChange;
           let decimal;
           var tokenAddress = 'token_' + String( tokenName ).toLowerCase() + '_address';
           var tokenDecimal = 'token_' + String( tokenName ).toLowerCase() + '_decimal';

            contractAddress         = sails.config.globals[tokenAddress];
            amountWithDecimalChange = amount * sails.config.globals[tokenDecimal];
            decimal                 = sails.config.globals[tokenDecimal];
           
           TokenUtil.getBalance(ETHUtil.getAddressFromPk(pk),contractAddress)
           .then((balance)=>{
              //step1:查询余额
              if(balance<amountWithDecimalChange)
              {
                  reject({error:"balance_not_enough",balance:CommonUtil.divide(balance,decimal).toString(),amount:amount});
              }
              else
              {
              
                //step2:转帐from,to,pk,gasPrice,amount,contractaddress
                TokenUtil.transfer(ETHUtil.getAddressFromPk(pk),to,pk,gasPrice,amountWithDecimalChange,contractAddress)
                .then((txid)=>{
                  resolve(txid);
                }).catch((exception)=>{
                  reject(exception);
                });
              }

           });
         }); 
       },
       balance: function(address,tokenName){
        return new Promise((resolve,reject)=>{

          let contractAddress;
          let decimal;

          var tokenAddress = 'token_' + String( tokenName ).toLowerCase() + '_address';
          var tokenDecimal = 'token_' + String( tokenName ).toLowerCase() + '_decimal';
          
          contractAddress = sails.config.globals[tokenAddress];
          decimal         = sails.config.globals[tokenDecimal];
          

          TokenUtil.getBalance( address , contractAddress ).then((balance)=>{
            console.log('balance',balance);
            return resolve(CommonUtil.divide(balance,decimal).toString());
          }).catch((exception)=>{
            return reject(exception);
          });

        })
         
       },
      getTransaction: function(txid) {
        sails.log.info("ETHService.getrawtransaction");
        return ;
      },
      getCurrentBlock:function(){
        return TokenUtil.getCurrentBlock();
      },
      validateAddress:function(address){
        return TokenUtil.validateaddress(address);
      }

}