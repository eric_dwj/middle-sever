let BatchUtil      = require('../utils/BatchUtil');
let TradeAssetUtil = require('../utils/TradeAssetUtil');
let DBUtil         = require('../utils/DBUtil');
let CommonUtil     = require('../utils/CommonUtil');
module.exports = {
      //注册的时候，将支付账号信息保存到redis里面
      addReceiveObjectToReceiveMQ: function(receiveObject){
        let assetsName              = 'assets_' + String( receiveObject.assetname ).toLowerCase() + '_name';
        let exchangeConfirmMQToken  = 'exchange_confirm_mq_'+String( receiveObject.assetname ).toLowerCase();
       
        return new Promise(async (resolve,reject)=>{
          console.log('receiveObject',receiveObject);
          try{
              await TradeAssetUtil.submitAssetTx(receiveObject,sails.config.asset.assets_side_deposit);
          }catch(exception){
              sails.log.error(exception);
          }
          
          if( receiveObject.assetname == sails.config.asset.assets_eth_name){  
            let blockNumber;
            try{
              blockNumber = await ETHService.getCurrentBlock();
            }catch(exception){
              sails.log.error(exception);
            }
            receiveObject.blockNumber = blockNumber;                
            BatchUtil.putToMQ(sails.config.mq.exchange_confirm_mq_eth, JSON.stringify(receiveObject))
            .then((result)=>{
                  resolve(result);
            });
          }else if( receiveObject.assetname == sails.config.asset.assets_btc_name){
            let blockNumber;
            try{
              blockNumber = await BTCService.getCurrentBlock();
              console.log('mark #### currentblock',blockNumber);
            }catch(exception){
              sails.log.error(exception);
            }

            receiveObject.blockNumber = blockNumber;
            console.log('mark #### receiveObject',receiveObject);
            BatchUtil.putToMQ(sails.config.mq.exchange_confirm_mq_btc, JSON.stringify(receiveObject))
            .then((result)=>{
                  resolve(result);
            });
          }else if( sails.config.asset[assetsName] ){
            let blockNumber;
            try{
              blockNumber = await TokenService.getCurrentBlock();
            }catch(exception){
              sails.log.error(exception);
            }
            receiveObject.blockNumber = blockNumber;
            BatchUtil.putToMQ(sails.config.mq[exchangeConfirmMQToken], JSON.stringify(receiveObject))
            .then((result)=>{
                  resolve(result);
            });   
          }
        });
      },
      run:function(){
         BatchBTCService.processExchangeReceive();
         BatchETHService.processExchangeReceive();
         BatchTokenService.processExchangeReceive();
      }
}