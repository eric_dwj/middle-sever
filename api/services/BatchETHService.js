/**
 * ETH的轮询任务，全部作为callback注入BatchReceiveService
 */

var ETHBlockComfirmation            = require('../classes/ETHBlockComfirmation.js');
var ETHBatchReceiveByAccount        = require('../classes/ETHBatchReceiveByAccount.js');
var ETHBatchReceiveByInWallet       = require('../classes/ETHBatchReceiveByInWallet.js');
var ETHSendConfirmBatch             = require('../classes/ETHSendConfirmBatch.js');
var ExchangeETHBlockConfirmation    = require('../classes/ExchangeETHBlockConfirmation.js');
var ExchangeETHSendConfirmBatch     = require('../classes/ExchangeETHSendConfirmBatch.js');
const  exchangeETHSendConfirmBatch  = new ExchangeETHSendConfirmBatch();
const  exchangeETHBlockConfirmation = new ExchangeETHBlockConfirmation();
const  ethSendConfirmBatch          = new ETHSendConfirmBatch();
const  ethBlockComfirmation         = new ETHBlockComfirmation();
const  ethBatchReceiveByAccount     = new ETHBatchReceiveByAccount();
const  ethBatchReceiveByInWallet    = new ETHBatchReceiveByInWallet();

module.exports = {
		processReceiveByAccount: function(){     
		  	BatchReceiveService.processReceiveByAccount( ethBatchReceiveByAccount );                    
		},
		processRecevieByInWallet:function(){
		  	BatchReceiveService.processRecevieByInWallet( ethBatchReceiveByInWallet );
		},
		processBlockComfirmation:function(){
		  	BatchReceiveService.processBlockComfirmation( ethBlockComfirmation );
		},
		processSend:function(){
			BatchSendService.processSend( ethSendConfirmBatch );
		},
		processExchangeReceive:function(){
			ExchangeBatchReceiveService.processReceiveByAccount( exchangeETHBlockConfirmation );
		},
        processExchangeSend:function(){
           ExchangeBatchSendService.processExchangeSend( exchangeETHSendConfirmBatch );
        }
}