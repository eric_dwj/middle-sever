
let CommonUtil                                = require('../utils/CommonUtil');
let ExchangeRecieveBlockConfirmationProcessor = require('../classes/ExchangeRecieveBlockConfirmationProcessor');
module.exports = {
    processReceiveByAccount:async function(batch){
      while( true ){
         var resp = await batch.getReceiveObjFromConfrimMQ()
         if( resp && resp.message ){
             let receiveObject = JSON.parse(resp.message);  
             try{
                var processor = new ExchangeRecieveBlockConfirmationProcessor(receiveObject,resp.id,batch)
                await processor.execute();
             }catch(exception)
             {
                sails.log.error("ReceiveByAccountProcesser.execute",exception);
             }
         }else{
            await CommonUtil.sleep(sails.config.mq.no_data_sleep);
         } 
      }
      
    }
}