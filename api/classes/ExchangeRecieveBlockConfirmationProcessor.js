
class ExchangeRecieveBlockConfirmationProcessor{
    constructor(receiveObject,mqid,batch){
      this.receiveObject = receiveObject;
      this.mqid          = mqid;
      this.batch         = batch;
      ExchangeRecieveBlockConfirmationProcessor.instance = this;
    }

    execute()
    {

      return new Promise(async(resolve,reject)=>{

        var  checkParaRes = await this.batch.checkInputData( this.receiveObject , this.mqid );
        if(  checkParaRes == false || checkParaRes == 'false' )
        {
          try
          {
            await this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
          }
          catch(exception){
            sails.log.error('removeReceiveObjectFromConfirmMQ :mqid ',this.mqid);
            sails.log.error(exception);

          }
          return reject({input_data_error:this.receiveObject,mqid:this.mqid});

        }

        let currentBlock;
        try
        {
          currentBlock = await this.batch.getCurrentBlock();
        }
        catch(exception){
            sails.log.error('batch.getCurrentBlock :currentBlock ',currentBlock);
            sails.log.error(exception);
            this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);

        }

        var isconfirmed = this.batch.isConfirm( this.receiveObject.blockNumber , currentBlock );
       
        if( ! isconfirmed )
        {
           return resolve(isconfirmed);
        }

        

        let updateBalanceRes;
        try
        {
          updateBalanceRes = await this.batch.updateBalance(this.receiveObject);
        }
        catch(exception){
            sails.log.error('batch.updateBalance',updateBalanceRes);
            sails.log.error(exception);
            this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
        }

        try
          {
            await this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
          }
          catch(exception){
            sails.log.error('removeReceiveObjectFromConfirmMQ :mqid ',this.mqid);
            sails.log.error(exception);

          }
          resolve(this.receiveObject);

      });
    }
    
}

module.exports = ExchangeRecieveBlockConfirmationProcessor;