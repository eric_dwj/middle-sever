let TradeUpdateBalance = require('../classes/TradeUpdateBalance');
let TradeInfo          = require('../classes/TradeInfo');
let TradePutlimit      = require('../classes/TradePutlimit');
let TradeCancelOrder   = require('../classes/TradeCancelOrder');
let TradeOrderBook     = require('../classes/TradeOrderBook');

class Trade{
	contructor(){
		Trade.instance = this;
	}

	orderBook( market , side , offset , limit )
	{
		   var    orderBook = new TradeOrderBook(  market, side , offset , limit );
           return orderBook.getOrderBook();
	}


	updateBalance( userid , asset , business , amount , detail )
	{
		   var    updateBalanceProcessor = new TradeUpdateBalance(userid,asset,business,amount,detail);
           return updateBalanceProcessor.updateBalance();
	}

	getBalance( userid )
	{
		
		   var    getBalanceProcessor = new TradeInfo(userid);
           return getBalanceProcessor.getBalance();
	}

	getOrder( userid )
	{
		   var    getBalanceProcessor = new TradeInfo(userid);
           return getBalanceProcessor.getOrder();
	}

	getAssetHistory( userid )
	{
		   var    getBalanceProcessor = new TradeInfo(userid);
           return getBalanceProcessor.getAssetHistory();
	}

	pubLimit( userid , market , side , amount , price  )
	{
		   var    putLimitProcessor = new TradePutlimit(userid,market,side,amount,price);
           return putLimitProcessor.pubLimit();
	}

	cancelOrder( userid , market ,orderid)
	{
			var cancelOrderProcessor = new TradeCancelOrder( userid , market , orderid);
			return cancelOrderProcessor.cancelOrder();
	}
}

module.exports = Trade;