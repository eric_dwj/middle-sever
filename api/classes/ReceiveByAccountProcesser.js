
let CommonUtil = require('../utils/CommonUtil');

class ReceiveByAccountProcesser
{

  constructor(receiveObject,mqid, batch) 
  {
    
    this.receiveObject = receiveObject;
    this.batch   = batch;
    this.mqid    = mqid;
  }

  

   execute(){
       return new Promise(async(resolve,reject)=>{
        
            var  checkParaRes =await this.batch.checkInputData( this.receiveObject,this.mqid );
            if(  checkParaRes ==false || checkParaRes =="false")
            {
              await this.removeReceiveObjectFromQueue(this.receiveObject.account,this.mqid);
              return reject({input_data_error:this.receiveObject,mqid:this.mqid});

            }

           let balance;
          
           try
           {    
                balance = await this.batch.getBalance( this.batch.getAddressFromAccount(this.receiveObject.account), this.receiveObject.userid ); 
           }
           catch(exception)
           {
              sails.log.error("batch.getBalance address: "+this.batch.getAddressFromAccount(this.receiveObject.account) +" userid: "+this.receiveObject.userid);
              sails.log.error(exception);
           }
           
           if( this.batch.isOvertime(this.receiveObject.timestamp) )
           {
                try
                {  
                  await this.removeReceiveObjectFromQueue(this.receiveObject.account,this.mqid);
                }
                catch(exception){
                sails.log.error("removeReceiveObjectFromQueue:  receiveObject"+this.receiveObject + " balance: "+balance);
                sails.log.error(exception);
                } 
           }
           
           if( !balance || this.batch.checkBalance(balance) )
           {
              return resolve(balance);
           } 

           let blockNumber = await this.batch.getCurrentBlock();

           this.receiveObject.balance     = balance;
           this.receiveObject.blockNumber = blockNumber;
           
           try
           {  
             var result1= await this.batch.addReceiveObjectToConfirmMQ(this.receiveObject);
           }
           catch(exception){
            sails.log.error("batch.addReceiveObjectToConfirmMQ: receiveObject"+this.receiveObject + " balance: "+balance);
            sails.log.error(exception);
            }

      
           try
           {  
             await this.removeReceiveObjectFromQueue(this.receiveObject.account,this.mqid);
           }
           catch(exception){
            sails.log.error("removeReceiveObjectFromQueue:  receiveObject"+this.receiveObject + " balance: "+balance);
            sails.log.error(exception);
            } 

           resolve(balance);



       });
   }

   async removeReceiveObjectFromQueue(account,id){
    return new Promise(async (resolve,reject)=>{
      var markRes   =  await this.batch.markNotInProcess(account);
      var removeRes =  await this.batch.removeReceiveObjectFromReceiveMQ(id);
      
      resolve({markRes:markRes,removeRes:removeRes});
    })
      
   }
}
module.exports = ReceiveByAccountProcesser;
