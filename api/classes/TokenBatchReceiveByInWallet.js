let BatchUtil  = require('../utils/BatchUtil');
let TokenUtil  = require('../utils/TokenUtil');
let AssetUtil  = require('../utils/AssetUtil');
let CommonUtil = require('../utils/CommonUtil');

class TokenBatchReceiveByInWallet{
     constructor() 
     {
      TokenBatchReceiveByInWallet.instance = this;
      this.receiveObject = null;
      this.index =0;
     }


    async checkInputData(receiveObject,mqid)
    {
      this.receiveObject = receiveObject;
      if( !mqid )
      {
        return false;
      }

      if( !receiveObject.timestamp )
      {
        return false;
      }

      if( !receiveObject.assetHistory )
      {
        return false;
      }

      if( !receiveObject.assetHistoryDate )
      {
        return false;
      }

      if( !receiveObject.userid )
      {
        return false;
      }

      if( !receiveObject.assetname )
      {
        return false;
      }

      if( !receiveObject.txid )
      {
        return false;
      }

      if( !receiveObject.account )
      {
        return false;
      }
      if( !receiveObject.balance )
      {
         return false;
      }


      if( !receiveObject.blockNumber )
      {
         return false;
      }

      let validateRes;

      var assetAccountName = String(receiveObject.assetname).toUpperCase() + 'Account';

      if( !receiveObject.account[assetAccountName] )
      {
         return false;
      }

      validateRes = await TokenUtil.validateaddress(receiveObject.account[assetAccountName].address)
      
      if( !validateRes )
      {
        return false;
      }

      return true;

    }

    getReceiveObjectFromInWalletMQ()
    {
        var inwalletMQName = 'inwallet_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
        // var confirmMQName = 'exchange_send_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
        this.index++;
        if( this.index ==sails.config.mq.tokens.length )
        {
          this.index = 0;
        }
        return BatchUtil.getFromMQ( sails.config.mq[inwalletMQName] ,10)
    }

    isConfirmed(txid){
      return new Promise(async (resolve,reject)=>{
          var trans = await TokenUtil.getTransactionReceipt(txid);
          if( trans)
          {
            resolve(true);
          }
          else
          {
            resolve(false);
          }
      });
    
    }

    updateAssetHistoryInRedis( userid , assetHistory , assetHistoryDate ){
        return AssetUtil.updateAssetHistoryInRedis( userid , assetHistory , assetHistoryDate , sails.config.asset.assets_history_state_deposited_checked );
    }

    updateAssetHistoryChecked(id){
        return AssetUtil.updateAssetHistory( id , sails.config.asset.assets_history_state_deposited_checked );
    }

    removeReceiveObjectFromInWalletMQ(id)
    {
        var inwalletMQName = 'inwallet_mq_'+String( this.receiveObject.assetname ).toLowerCase();
        return BatchUtil.removeFromMQ( sails.config.mq[inwalletMQName] ,id )
    }


}

module.exports = TokenBatchReceiveByInWallet;