let BatchUtil  = require('../utils/BatchUtil');
let ETHUtil    = require('../utils/ETHUtil');
let AssetUtil  = require('../utils/AssetUtil');
let CommonUtil = require('../utils/CommonUtil');


class ETHBatchReceiveByAccount{

    constructor() {
      ETHBatchReceiveByAccount.instance = this;
    }

    getAddressFromAccount(account)
    {
      
        return account.ETHAccount.address;
    }

    async checkInputData(receiveObject,mqid)
    {

      

      if( !mqid )
      {
        return false;
      }

      if( !receiveObject.timestamp )
      {
        return false;
      }

      if( !receiveObject.userid )
      {
        return false;
      }

      if( !receiveObject.assetname )
      {
        return false;
      }

      if( !receiveObject.account )
      {
        return false;
      }

      if( !receiveObject.account.ETHAccount )
      {
        return false;
      }

      let validateRes;

      validateRes = await ETHUtil.validateaddress(receiveObject.account.ETHAccount.address)
      
      if( !validateRes )
      {
        return false;
      }
    

      return true;

    }

    getCurrentBlock()
    {
        return ETHService.getCurrentBlock();
    }

    markNotInProcess(account){
      // sails.log.debug("this.markNotInProcess",account);
      return BatchUtil.markAccountNotInProcess(account.ETHAccount.address);
    }

    isOvertime(timestamp){

        // sails.log.debug( new Date().getTime() -parseInt(timestamp) );
        // sails.log.debug( sails.config.mq.on_receive_ttl);
       if(sails.config.mq.on_receive_ttl < new Date().getTime() -parseInt(timestamp))
       {
        return true
       }else
       {
        return false;
       }
    }

     getReceiveObjectFromReceiveMQ()
    {
       return BatchUtil.getFromMQ( sails.config.mq.receive_mq_eth ,10)
    }

    getBalance(address,userid) 
    {
        return new Promise((resolve,reject)=>{
          ETHService.balance( address ).then((balance)=>{
            sails.log.debug("balance : "+balance+" address: "+address);
              return resolve(balance);
          });
        }) 
    }

    checkBalance(balance) 
    {     
         if( !balance )
         {
          return false;
         }
        
         if( balance < sails.config.globals.eth_receive_threshold )
         {
             
            return true;
         }
         else
         {
            return false;
         }
    }

    addReceiveObjectToConfirmMQ(receiveObject)
    {
        return BatchUtil.putToMQ(sails.config.mq.confirm_mq_eth,JSON.stringify(receiveObject));

    }
    
   removeReceiveObjectFromReceiveMQ(id)
   {
        return BatchUtil.removeFromMQ(sails.config.mq.receive_mq_eth,id);
   }



    
};


module.exports = ETHBatchReceiveByAccount;