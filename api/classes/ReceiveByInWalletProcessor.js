
let CommonUtil = require('../utils/CommonUtil');

class ReceiveByInWalletProcessor{

    constructor(receiveObject,mqid, batch) 
    {
      
      this.receiveObject = receiveObject;
      this.batch   = batch;
      this.mqid    = mqid;
    }
    async execute(){
  　　return new Promise(async(resolve,reject)=>{　
      var  checkParaRes = await this.batch.checkInputData( this.receiveObject , this.mqid );
      
      if(  checkParaRes == false || checkParaRes == 'false' )
      {
        try
        {
          await this.batch.removeReceiveObjectFromInWalletMQ(this.mqid);
        }
        catch(exception){
          sails.log.error('removeReceiveObjectFromConfirmMQ :mqid ',this.mqid);
          sails.log.error(exception);
  
        }
        return reject({input_data_error:this.receiveObject,mqid:this.mqid});
  
      }
       let isConfirmed;
       try
       {
         isConfirmed = await this.batch.isConfirmed(this.receiveObject.txid);
       }
       catch(exception){
          sails.log.error('batch.getTransatction ',this.receiveObject.txid);
          sails.log.error(exception);
          this.batch.removeReceiveObjectFromInWalletMQ(this.mqid);
        }

  
        if( isConfirmed )
        {
          let assetHistory;
  
          try
          {
            assetHistory = await this.batch.updateAssetHistoryChecked(this.receiveObject.assetHistory.id);
          }
          catch(exception){
            sails.log.error("batch.updateAssetHistoryChecked",exception);
          }

  
          let assetHistoryRedis;
  
          try
          {
            assetHistoryRedis =await this.batch.updateAssetHistoryInRedis(this.receiveObject.userid,assetHistory,this.receiveObject.assetHistoryDate);
          }
          catch(exception){
            sails.log.error("batch.updateAssetHistoryChecked",exception);
          }

  
          try
          {
            await this.batch.removeReceiveObjectFromInWalletMQ(this.mqid);
          }
          catch(exception){
            sails.log.error("batch.removeReceiveObjectFromInWalletMQ",exception);
          }
  
          sails.log.debug("####### confirmed##########",isConfirmed);
          resolve(isConfirmed);
  
        }
        else
        {
            sails.log.debug("#######not confirmed##########",isConfirmed);
            resolve(isConfirmed);
        }
    });
    }
  }
module.exports = ReceiveByInWalletProcessor;
  