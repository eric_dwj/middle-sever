let BatchUtil    = require('../utils/BatchUtil');
let ETHUtil      = require('../utils/ETHUtil');
let AssetUtil    = require('../utils/AssetUtil');
let CommonUtil   = require('../utils/CommonUtil');
var EventEmitter = require('events').EventEmitter;

class ETHSendConfirmBatch{
    
    constructor() 
    {
      ETHSendConfirmBatch.instance = this;
      this.isConfirmFinished       = false;
      this.ev                      = new EventEmitter;
      this.startConfirmSchedule();
    }

    startConfirmSchedule(){

        let txid;      
        this.ev.on('txid', (emit_txid)=>{
          txid =emit_txid;
          console.log('txid =emit_txid',txid);
        });
        CommonUtil.scheduler(sails.config.mq.send_confirm_fq,
        async ()=>{
          if( !this.isConfirmFinished && txid  )
          {
            var trans = await ETHUtil.getTransactionReceipt(txid);
          
            if( trans )
            {
               this.isConfirmFinished =true;
               this.ev.emit('confirm', txid);
               txid = null;
            } 
          }
        },'startConfirmSchedule');
    }

    isConfirm(tx)
    {
      this.ev.emit('txid', tx);
      return new Promise((resolve,reject)=>{
        this.ev.on('confirm',(txid)=>{
          console.log('confirm',txid);
            resolve(txid);
        })
      })
    }

    getSendObjFromConfrimMQ()
    {
        this.isConfirmFinished = false;
        return BatchUtil.getFromMQ( sails.config.mq.send_mq_eth ,10)
    }


    async checkInputData(sendObject,mqid)
    {

      if( !mqid )
      {
        return false;
      }
      
      if( !sendObject.timestamp )
      {
        return false;
      }

      if( !sendObject.userid )
      {
        return false;
      }

      if( !sendObject.assetname )
      {
        return false;
      }

      if( !sendObject.address )
      {
        return false;
      }

      if( !sendObject.size )
      {
         return false;
      }

      let validateRes;

      validateRes = await ETHUtil.validateaddress(sendObject.address)
      
      if( !validateRes )
      {
        return false;
      }

      return true;

    }

    updateAsset( userid , size )
    {
      var asset = {};
      asset.userid          = userid;
      asset.ethAvailable    = size;
      return AssetUtil.updateAssetSend( userid , asset ,'ethAvailable' );
    }

    updateAssetInRedis(userid,asset)
    {
        return AssetUtil.updateAssetInRedis(userid,asset);
    }

    initAssetHistory(userid,size,txid)
    {
        return AssetUtil.initAssetHistoryUnchecked
       (
        userid,
        sails.config.asset.assets_eth_name,
        size,
        txid,
        sails.config.asset.assets_history_side_withdraw,
        sails.config.asset.assets_history_state_withdraw_unchecked
        );
    }

    initAssetHistoryInRedis(userid,assetHistory,assetHistoryDate)
    {
        return AssetUtil.updateAssetHistoryInRedis(
                                                    userid,
                                                    assetHistory,
                                                    assetHistoryDate,
                                                    sails.config.asset.assets_history_state_withdraw_unchecked
                                                    );
    }

    removeSendObjectFromMQ(id)
    {
      
        return BatchUtil.removeFromMQ(sails.config.mq.send_mq_eth,id);
    }

    sendToAddress(address,size)
    {


        var gasPrice       = ETHUtil.getSendGasPriceByGwei();
        var pk             = sails.config.globals.eth_out_privatekey;
        var to             = address; 
        var amount         = size- sails.config.globals.eth_send_threshold;
      
        let result;
        try
        {
            result = ETHService.transferWithBalanceCheck(to,pk,gasPrice,amount);
        }catch(exception){
            sails.log.debug('ETHService.transferWithBalanceCheck account:'+account +' balance: '+balance);
            sails.log.debug(exception);
        }

        return result;
    }

    updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate){
        return AssetUtil.updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate,sails.config.asset.assets_history_state_withdraw_checked );
    }

    updateAssetHistoryChecked(id){
        return AssetUtil.updateAssetHistory( id , sails.config.asset.assets_history_state_withdraw_checked );
    }

}

module.exports = ETHSendConfirmBatch;
