let BatchUtil    = require('../utils/BatchUtil');
let TokenUtil      = require('../utils/TokenUtil');
let AssetUtil    = require('../utils/AssetUtil');
let CommonUtil   = require('../utils/CommonUtil');
var EventEmitter = require('events').EventEmitter;

class TokenSendConfirmBatch{
    
    constructor() 
    {
      TokenSendConfirmBatch.instance = this;
      this.isConfirmFinished       = false;
      this.ev                      = new EventEmitter;
      this.sendObject              = null;
      this.index                   = 0;
      this.startConfirmSchedule();
    }

   startConfirmSchedule(){

        let txid;      
        this.ev.on('txid', (emit_txid)=>{
          txid =emit_txid;
          
        });
        CommonUtil.scheduler(sails.config.mq.send_confirm_fq,
        async ()=>{
          if( !this.isConfirmFinished && txid  )
          {
            var trans = await TokenUtil.getTransactionReceipt(txid);
          
            if( trans )
            {
               this.isConfirmFinished =true;
               this.ev.emit('confirm', txid);
               txid = null;
            } 
          }
        },'startConfirmSchedule');
    }

    isConfirm(tx)
    {
      this.ev.emit('txid', tx);
      return new Promise((resolve,reject)=>{
        this.ev.on('confirm',(txid)=>{
            resolve(txid);
        })
      })
    }

    getSendObjFromConfrimMQ()
    {
        this.isConfirmFinished = false;
        var sendMQName = 'send_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
        // var confirmMQName = 'exchange_send_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
        this.index++;
        if( this.index ==sails.config.mq.tokens.length )
        {
          this.index = 0;
        }

        return BatchUtil.getFromMQ( sails.config.mq[sendMQName] ,10)
    }


    async checkInputData(sendObject,mqid)
    {
      this.sendObject = sendObject;
      
      if( !mqid )
      {
        return false;
      }
      
      if( !sendObject.timestamp )
      {
        return false;
      }

      if( !sendObject.userid )
      {
        return false;
      }

      if( !sendObject.assetname )
      {
        return false;
      }

      if( !sendObject.address )
      {
        return false;
      }

      if( !sendObject.size )
      {
         return false;
      }

      let validateRes;

      validateRes = await TokenUtil.validateaddress(sendObject.address)
      
      if( !validateRes )
      {
        return false;
      }

      return true;

    }

    updateAsset( userid , size )
    {
      var asset = {};
      asset.userid              = userid;
      var assetAvailableName    = String( this.sendObject.assetname ).toLowerCase() +'Available';
      asset[assetAvailableName] = size;
   
      
      
      return AssetUtil.updateAssetSend( userid , asset ,assetAvailableName);
    }

    updateAssetInRedis(userid,asset)
    {
        return AssetUtil.updateAssetInRedis(userid,asset);
    }

    initAssetHistory(userid,size,txid)
    {
        return AssetUtil.initAssetHistoryUnchecked
       (
        userid,
        this.sendObject.assetname,
        size,
        txid,
        sails.config.asset.assets_history_side_withdraw,
        sails.config.asset.assets_history_state_withdraw_unchecked
        );
    }

    initAssetHistoryInRedis(userid,assetHistory,assetHistoryDate)
    {
        return AssetUtil.updateAssetHistoryInRedis(
                                                    userid,
                                                    assetHistory,
                                                    assetHistoryDate,
                                                    sails.config.asset.assets_history_state_withdraw_unchecked
                                                    );
    }

    removeSendObjectFromMQ(id)
    {
        var sendMQName = 'send_mq_'+String( this.sendObject.assetname ).toLowerCase();
        return BatchUtil.removeFromMQ(sails.config.mq[sendMQName],id);
    }

    sendToAddress(address,size)
    {

        var tokenSendThredsholdName = 'token_' + String( this.sendObject.assetname ).toLowerCase() +'_send_threshold';
        var gasPrice       = TokenUtil.getSendGasPriceByGwei();
        var pk             = sails.config.globals.token_out_privatekey;
        var to             = address; 
        var amount         = size-sails.config.globals[tokenSendThredsholdName];
      
        let result;
        try
        {

            result = TokenService.transferWithBalanceCheck(to,pk,gasPrice,amount,this.sendObject.assetname);
        }catch(exception){
            sails.log.debug('TokenService.transferWithBalanceCheck account:'+account +' balance: '+balance);
            sails.log.debug(exception);
        }

        return result;
    }

    updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate){
        return AssetUtil.updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate,sails.config.asset.assets_history_state_withdraw_checked );
    }

    updateAssetHistoryChecked(id){
        return AssetUtil.updateAssetHistory( id , sails.config.asset.assets_history_state_withdraw_checked );
    }

}

module.exports = TokenSendConfirmBatch;
