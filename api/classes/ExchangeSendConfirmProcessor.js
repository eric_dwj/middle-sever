let AssetUtil    = require('../utils/AssetUtil');
let CommonUtil   = require('../utils/CommonUtil');

class ExchangeSendConfirmProcessor{

    constructor(sendObject, mqid, batch) 
    {
      this.sendObject = sendObject;
      this.batch      = batch;
      this.mqid       = mqid;
    }
    
    execute(){

    return new Promise(async(resolve,reject)=>{

      var checkParaRes = await this.batch.checkInputData( this.sendObject , this.mqid );
      
      if(  checkParaRes == false || checkParaRes == 'false' )
      {
        try{
            await this.batch.removeSendObjectFromMQ(this.mqid);
        }catch(exception){
            sails.log.error('batch.removeSendObjectFromMQ',exception);
        }
          return reject({ input_data_error : this.sendObject , mqid : this.mqid });
      }

      let txid;
      try{
          txid = await this.batch.sendToAddress( this.sendObject.address , this.sendObject.size );
      }catch(exception){
          sails.log.error('batch.sendToAddress',exception);
          this.batch.removeSendObjectFromMQ(this.mqid);
      }

      try{
          await this.batch.createAssetTx(txid);
      }catch(exception){
          sails.log.error(exception);
          this.batch.removeSendObjectFromMQ(this.mqid);
      }

      if( txid )
      {
        this.sendObject.detail = txid;
        let updateBalanceRes;
        try{
          updateBalanceRes = await this.batch.updateBalance(this.sendObject);
        }catch(exception){
            sails.log.error(exception);
            this.batch.removeSendObjectFromMQ(this.mqid);
        }
      }
      
     
      
      try{
          await this.batch.isConfirm(txid);
      }catch(exception){
          sails.log.error('batch.isConfirm',exception);
          this.batch.removeSendObjectFromMQ(this.mqid);
      }


      try{
          await this.batch.removeSendObjectFromMQ(this.mqid);
      }catch(exception){
          sails.log.error('batch.removeSendObjectFromMQ',exception);
      }
      resolve(this.sendObject);
        
    });
  }
}

module.exports = ExchangeSendConfirmProcessor;