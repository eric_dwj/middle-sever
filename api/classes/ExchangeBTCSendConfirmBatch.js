let BatchUtil      = require('../utils/BatchUtil');
let BTCUtil        = require('../utils/BTCUtil');
let AssetUtil      = require('../utils/AssetUtil');
let CommonUtil     = require('../utils/CommonUtil');
let TradeAssetUtil = require('../utils/TradeAssetUtil');
let TradeUpdateBalance = require('../classes/TradeUpdateBalance');

class ExchangeBTCSendConfirmBatch{
    
    constructor() {
      ExchangeBTCSendConfirmBatch.instance = this;
    }

    isConfirm(tx){
      console.log(tx);
      return new Promise(async (resolve,reject)=>{
        if( !tx ){
          reject({error:'tx_null_exception'})
        }
        try{
            let trans = await BTCUtil.getrawtransaction(tx);
            while(true){
              if( trans && trans.txid ){
                console.log('resolve',tx);
                return resolve(tx);
              }else{
                console.log('##no end');
                CommonUtil.sleep(sails.config.asset.no_data_sleep);
                trans = await BTCUtil.getrawtransaction(tx);
              } 
            }
        }catch(exception){
          sails.log.error(exception);
        }
      });
    }
    
    updateBalance(sendObject)
    {
        var    updateBalanceProcessor = new TradeUpdateBalance(sendObject.userid,sendObject.assetname,sails.config.trader.business_withdraw,String(sendObject.size),sendObject.detail);
        return updateBalanceProcessor.updateBalance();
    }

    getSendObjFromConfrimMQ(){
        this.isSubmitFinished = false;
        return BatchUtil.getFromMQ( sails.config.mq.exchange_send_mq_btc ,10)
    }


    async checkInputData(sendObject,mqid){
      if( !mqid ){
        return false;
      }

      console.log('##checkInputData mqid');
      
      if( !sendObject.detail ){
        return false;
      }

      console.log('##checkInputData detail');
      
      if( !sendObject.timestamp ){
        return false;
      }

      console.log('##checkInputData timestamp');

      if( !sendObject.userid ){
        return false;
      }
      
      console.log('##checkInputData userid');

      if( !sendObject.assetname ){
        return false;
      }

      console.log('##checkInputData assetname');

      if( !sendObject.address ){
        return false;
      }

      if( !sendObject.size ){
         return false;
      }

      console.log('##checkInputData size');

      let validateRes = await BTCUtil.validateaddress(sendObject.address)
   
      console.log('##checkInputData validateRes');
      
      if( !validateRes ){
        return false;
      }
      
      this.sendObject = sendObject;

      return true;

    }

    removeSendObjectFromMQ( id ){
        return BatchUtil.removeFromMQ(sails.config.mq.exchange_send_mq_btc,id);
    }

    createAssetTx( txid ){
          this.sendObject.tx = txid; 
          try{
              return TradeAssetUtil.submitAssetTx(this.sendObject,sails.config.asset.assets_side_withdraw);
          }catch(exception){
              sails.log.error(exception);
          }
    }
    
    sendToAddress(address,size){
        var transactionFee = sails.config.globals.btc_send_transferfee;
        var pk             = sails.config.globals.btc_out_privatekey;
        var to             = address; 
        var amount         = size- sails.config.globals.btc_send_threshold;
      
        let result;
        try{
            result = BTCService.transferWithBalanceCheck(to,amount,pk,transactionFee);
        }catch(exception){
            sails.log.debug('BTCService.transferWithBalanceCheck address:'+address +' balance: '+balance);
            sails.log.debug(exception);
        }
        return result;
    }


}

module.exports = ExchangeBTCSendConfirmBatch;
