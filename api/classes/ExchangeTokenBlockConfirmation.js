
let TradeAssetUtil     = require('../utils/TradeAssetUtil');
let TradeUpdateBalance = require('../classes/TradeUpdateBalance');
let TokenService       = require('../services/TokenService');
let BatchUtil          = require('../utils/BatchUtil');
class ExchangeTokenBlockConfirmation{
    constructor(){
      ExchangeTokenBlockConfirmation.instance = this;
      this.receiveObject = null;
      this.index         = 0;

    }



    async checkInputData(receiveObject,mqid){

       this.receiveObject = receiveObject;
       console.log('receiveObject',receiveObject);

      if( !mqid )
      {
        return false;
      }


      if( !receiveObject.timestamp )
      {
        return false;
      }
      
      if( !receiveObject.userid )
      {
        return false;
      }

      if( !receiveObject.assetname )
      {
        return false;
      }
    
      if( !receiveObject.size )
      {
         return false;
      }
      
      if( !receiveObject.detail )
      {
         return false;
      }

      if( !receiveObject.blockNumber )
      {
         return false;
      }

      if( !receiveObject.tx )
      {
         return false;
      }

     

      return true;

    }
    
    getCurrentBlock()
    {
        return TokenService.getCurrentBlock();
    }
 
    updateBalance(receiveObject)
    {
        var    updateBalanceProcessor = new TradeUpdateBalance(receiveObject.userid,receiveObject.assetname,sails.config.trader.business_deposit,receiveObject.size,receiveObject.detail);
        return updateBalanceProcessor.updateBalance();
    }


    isConfirm(transactionblock,currentBlock)
    {
      sails.log.debug('transactionblock:'+transactionblock+'  currentBlock:'+currentBlock+' confirm:'+(currentBlock-transactionblock));    
      this.receiveObject.confirmedBlock     = currentBlock-transactionblock;
      this.receiveObject.confirmBlockNumber = sails.config.globals.token_confirm_block_number;
      TradeAssetUtil.addChangedInfoToMq(this.receiveObject.userid,null,null,null,null,null,null,this.receiveObject);
      if( currentBlock -transactionblock >= sails.config.globals.token_confirm_block_number ){
        TradeAssetUtil.updateAssetTx(this.receiveObject.tx,sails.config.asset.tx_confirmed);
        return true;
      }else{
        return false;
      }
    }
    
    removeReceiveObjectFromConfirmMQ(id)
    {
        var confirmMQName = 'exchange_confirm_mq_'+String( this.receiveObject.assetname ).toLowerCase();
        return BatchUtil.removeFromMQ(sails.config.mq[confirmMQName],id);
    }

    getReceiveObjFromConfrimMQ()
    {
        var confirmMQName = 'exchange_confirm_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
        
        this.index++;
        if( this.index ==sails.config.mq.tokens.length )
        {
          this.index = 0;
        }
        return BatchUtil.getFromMQ( sails.config.mq[confirmMQName] ,10)

    }
}

module.exports = ExchangeTokenBlockConfirmation;