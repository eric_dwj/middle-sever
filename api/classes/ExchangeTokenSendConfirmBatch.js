let BatchUtil          = require('../utils/BatchUtil');
let TokenUtil          = require('../utils/TokenUtil');
let AssetUtil          = require('../utils/AssetUtil');
let CommonUtil         = require('../utils/CommonUtil');
let TradeAssetUtil     = require('../utils/TradeAssetUtil');
let TradeUpdateBalance = require('../classes/TradeUpdateBalance');
class ExchangeTokenSendConfirmBatch{
    constructor() {
      ExchangeTokenSendConfirmBatch.instance = this;
      this.index  = 0;
      this.init();
    }

    async init(){
      this.nounce = await TokenUtil.getTransactionCount( TokenUtil.getAddressFromPk( sails.config.globals.token_out_privatekey ));
      console.log( '###############' , this.nounce );
    }
    isConfirm(tx){  
    console.log(tx);      
      return new Promise( async (resolve,reject)=>{
        if( !tx ){
          reject({error:'tx_null_exception'})
        }
        while(true){
          var trans         = await TokenUtil.getTransactionReceipt(tx);
          let currentNounce = await TokenUtil.getTransactionCount( TokenUtil.getAddressFromPk( sails.config.globals.token_out_privatekey ));
          if( trans && currentNounce == this.nounce +1 ){
              this.nounce = currentNounce;
              await TradeAssetUtil.updateAssetTx( tx , sails.config.asset.tx_confirmed );
               console.log('resolve',tx);
              return resolve( tx );
          }else{
              console.log('##no end');
              CommonUtil.sleep( sails.config.asset.no_data_sleep );
              trans = await TokenUtil.getTransactionReceipt( tx );
          }
        }        
      });
    }
    createAssetTx(txid){
          this.sendObject.tx = txid; 
          try{
              return TradeAssetUtil.submitAssetTx(this.sendObject,sails.config.asset.assets_side_withdraw);
          }catch(exception){
              sails.log.error(exception);
          }
    } 
    updateBalance(sendObject)
    {
        var    updateBalanceProcessor = new TradeUpdateBalance(sendObject.userid,sendObject.assetname,sails.config.trader.business_withdraw,String(sendObject.size),sendObject.detail);
        return updateBalanceProcessor.updateBalance();
    }

    getSendObjFromConfrimMQ()
    {
        this.isConfirmFinished = false;
        var confirmMQName = 'exchange_send_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
        this.index++;
        if( this.index ==sails.config.mq.tokens.length ){
          this.index = 0;
        }

        return BatchUtil.getFromMQ( sails.config.mq[confirmMQName] ,10)
    }


    async checkInputData(sendObject,mqid)
    {
      this.sendObject = sendObject;
      console.log('this.sendObject',this.sendObject);

      if( !this.nounce ){
        this.nounce  = await TokenUtil.getTransactionCount( TokenUtil.getAddressFromPk(sails.config.globals.eth_out_privatekey) );
      }

      if( !mqid ){
        return false;
      }
      
      if( !sendObject.detail ){
        return false;
      }
      
      if( !sendObject.timestamp ){
        return false;
      }

      if( !sendObject.userid ){
        return false;
      }

      if( !sendObject.assetname ){
        return false;
      }

      if( !sendObject.address ){
        return false;
      }

      if( !sendObject.size ){
         return false;
      }

      let validateRes;

      validateRes = await TokenUtil.validateaddress(sendObject.address)
      
      if( !validateRes ){
        return false;
      }

      return true;

    }


    removeSendObjectFromMQ(id)
    {
        
        var confirmMQName = 'exchange_send_mq_'+String( this.sendObject.assetname ).toLowerCase();
        return BatchUtil.removeFromMQ(sails.config.mq[confirmMQName],id);
    }

    sendToAddress(address,size)
    {
        var tokenSendThredsholdName = 'token_' + String( this.sendObject.assetname ).toLowerCase() +'_send_threshold';
        var gasPrice                = TokenUtil.getSendGasPriceByGwei();
        var pk                      = sails.config.globals.token_out_privatekey;
        var to                      = address; 
        var amount                  = size-sails.config.globals[tokenSendThredsholdName];

        let result;
        try
        {

            result = TokenService.transferWithBalanceCheck(to,pk,gasPrice,amount,this.sendObject.assetname);
        }catch(exception){
            sails.log.debug('TokenService.transferWithBalanceCheck account:'+account +' balance: '+balance);
            sails.log.debug(exception);
        }

        return result;
    }


}

module.exports = ExchangeTokenSendConfirmBatch;
