let BatchUtil    = require('../utils/BatchUtil');
let ETHUtil      = require('../utils/ETHUtil');
let CommonUtil   = require('../utils/CommonUtil');
let TradeAssetUtil = require('../utils/TradeAssetUtil');
let TradeUpdateBalance = require('../classes/TradeUpdateBalance');

class ExchangeETHSendConfirmBatch{
    
    constructor() {
      ExchangeETHSendConfirmBatch.instance = this;
      this.init();
    }

    async init(){
      this.nounce = await ETHUtil.getTransactionCount( ETHUtil.getAddressFromPk(sails.config.globals.eth_out_privatekey));
      console.log( '###############' , this.nounce );
    }

    isConfirm(tx){
      console.log(tx);
      return new Promise( async (resolve,reject) => {
        if( !tx ){
          reject({error:'tx_null_exception'})
        }
        while(true){
          let trans         = await ETHUtil.getTransactionReceipt( tx );
          let currentNounce = await ETHUtil.getTransactionCount( ETHUtil.getAddressFromPk(sails.config.globals.eth_out_privatekey) );
          if( trans && currentNounce == this.nounce +1 ){
              this.nounce = currentNounce;
              await TradeAssetUtil.updateAssetTx( tx , sails.config.asset.tx_confirmed );
              console.log('resolve',tx);
              return resolve(tx);
          }else{
              console.log('##no end');
              CommonUtil.sleep( sails.config.asset.no_data_sleep );
              trans = await ETHUtil.getTransactionReceipt( tx );
          }
        }     
      })
    }

    async checkInputData(sendObject,mqid){
      if( !this.nounce ){
        this.nounce  = await ETHUtil.getTransactionCount( ETHUtil.getAddressFromPk(sails.config.globals.eth_out_privatekey) );
      }
      if( !mqid ){
        return false;
      }
      if( !sendObject.timestamp ){
        return false;
      }
      if( !sendObject.detail ){
        return false;
      }
      if( !sendObject.userid ){
        return false;
      }
      if( !sendObject.assetname ){
        return false;
      }
      if( !sendObject.address ){
        return false;
      }
      if( !sendObject.size ){
         return false;
      }
      let validateRes;
      validateRes = await ETHUtil.validateaddress(sendObject.address)
      if( !validateRes ){
        return false;
      }
      this.sendObject = sendObject;
      return true;
    }

    updateBalance(sendObject){
        var    updateBalanceProcessor = new TradeUpdateBalance(sendObject.userid,sendObject.assetname,sails.config.trader.business_withdraw,String(sendObject.size),sendObject.detail);
        return updateBalanceProcessor.updateBalance();
    }

    removeSendObjectFromMQ(id){
        return BatchUtil.removeFromMQ(sails.config.mq.exchange_send_mq_eth,id);
    }

    getSendObjFromConfrimMQ(){
        return BatchUtil.getFromMQ( sails.config.mq.exchange_send_mq_eth ,10)
    }

    sendToAddress(address,size){
        var gasPrice       = ETHUtil.getSendGasPriceByGwei();
        var pk             = sails.config.globals.eth_out_privatekey;
        var to             = address; 
        var amount         = size- sails.config.globals.eth_send_threshold;
        let result;
        try{
            result = ETHService.transferWithBalanceCheck(to,pk,gasPrice,amount);
        }catch(exception){
            sails.log.error(exception);
        }
        return result;
    }

    createAssetTx(txid){
        this.sendObject.tx = txid; 
        try{
            return TradeAssetUtil.submitAssetTx(this.sendObject,sails.config.asset.assets_side_withdraw);
        }catch(exception){
            sails.log.error(exception);
        }
    }
}

module.exports = ExchangeETHSendConfirmBatch;