let BatchUtil  = require('../utils/BatchUtil');
let BTCUtil    = require('../utils/BTCUtil');
let AssetUtil  = require('../utils/AssetUtil');
let CommonUtil = require('../utils/CommonUtil');

class BTCBlockComfirmation{
    constructor() {
      BTCBlockComfirmation.instance = this;
    }

     
    getAccount(account)
    { 
      return account.BTCAccount;
    }

    getReceiveObjFromConfrimMQ()
    {
        return BatchUtil.getFromMQ( sails.config.mq.confirm_mq_btc ,10)
    }


    async checkInputData(receiveObject,mqid)
    {
    
      if( !mqid )
      {
        return false;
      }

      if( !receiveObject.timestamp )
      {
        return false;
      }
      
      if( !receiveObject.userid )
      {
        return false;
      }

    
      if( !receiveObject.assetname )
      {
        return false;
      }
     
      if( !receiveObject.account )
      {
        return false;
      }
 
      if( !receiveObject.balance )
      {
         return false;
      }

      if( !receiveObject.blockNumber )
      {
         return false;
      }

      let validateRes;

      validateRes = await BTCUtil.validateaddress(receiveObject.account.BTCAccount.address)
  
      if( !validateRes )
      {
        return false;
      }

      return true;

    }

    getCurrentBlock()
    {
        return BTCService.getCurrentBlock();
    }


    isConfirm(transactionblock,currentBlock)
    {
        if( currentBlock -transactionblock >= sails.config.globals.btc_confirm_block_number )
        {
            return true;
        }else
        {
            return false;
        }
    }


     initAssetHistory(userid,balance,txid)
    {
       return AssetUtil.initAssetHistoryUnchecked(
        userid,
        sails.config.asset.assets_btc_name,
        balance,
        txid,
        sails.config.asset.assets_history_side_deposit,
        sails.config.asset.assets_history_state_deposited_unchecked
        );
    }

    updateAsset(userid,balance)
    {
      var asset = {};
      asset.userid          = userid;
      asset.btcAvailable    = balance;
      return AssetUtil.updateAssetReceive( userid , asset ,'btcAvailable');
    }

    initAssetHistoryInRedis(userid,assetHistory,assetHistoryDate)
    {
        return AssetUtil.updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate,sails.config.asset.assets_history_state_deposited_unchecked);
    }

    updateAssetInRedis(userid,asset)
    {
        return AssetUtil.updateAssetInRedis(userid,asset);
    }


    removeReceiveObjectFromConfirmMQ(id)
    {
      
        return BatchUtil.removeFromMQ(sails.config.mq.confirm_mq_btc,id);
    }
    addReceiveObjectToInWalletMQ(receiveObject)
    {
        return BatchUtil.putToMQ(sails.config.mq.inwallet_mq_btc,JSON.stringify(receiveObject));
    }


    sendToInWallet(account,balance)
    {
        var transactionFee = sails.config.globals.btc_receive_transferfee;
        var pk             = account.privateKey;
        var to             = sails.config.globals.btc_in_address; 
        var amount         = balance- sails.config.globals.btc_receive_transferfee;
      
        let result;
        try
        {
            result = BTCService.transferWithBalanceCheck(to,amount,pk,transactionFee);
        }catch(exception){
            sails.log.debug('BTCService.transferWithBalanceCheck account:'+account +' balance: '+balance);
            sails.log.debug(exception);
        }

        return result;
    }

}

module.exports = BTCBlockComfirmation;
