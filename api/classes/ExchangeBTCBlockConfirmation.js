let TradeAssetUtil     = require('../utils/TradeAssetUtil');
let TradeUpdateBalance = require('../classes/TradeUpdateBalance');
let BTCService         = require('../services/BTCService');
let BatchUtil          = require('../utils/BatchUtil');

class ExchangeBTCBlockConfirmation{
    constructor(){
      ExchangeBTCBlockConfirmation.instance = this;
      this.receiveObject = null;
    }



    async checkInputData(receiveObject,mqid){
      console.log('mark #### checkInputData',receiveObject,mqid)
      if( !mqid ){
        return false;
      }

      if( !receiveObject.timestamp ){
        return false;
      }
      
      if( !receiveObject.userid ){
        return false;
      }

      if( !receiveObject.assetname ){
        return false;
      }
    
      if( !receiveObject.size ){
         return false;
      }
      
      if( !receiveObject.detail ){
         return false;
      }

      if( !receiveObject.blockNumber ){
         return false;
      }

      if( !receiveObject.tx ){
         return false;
      }

      this.receiveObject = receiveObject;

      return true;

    }
    
    getCurrentBlock(){
        return BTCService.getCurrentBlock();
    }
    
    updateBalance(receiveObject){
        var    updateBalanceProcessor = new TradeUpdateBalance(receiveObject.userid,receiveObject.assetname,sails.config.trader.business_deposit,receiveObject.size,receiveObject.detail);
        return updateBalanceProcessor.updateBalance();
    }

    isConfirm(transactionblock,currentBlock){ 
        this.receiveObject.confirmedBlock     = currentBlock-transactionblock;
        this.receiveObject.confirmBlockNumber = sails.config.globals.btc_confirm_block_number;
        
        TradeAssetUtil.addChangedInfoToMq(this.receiveObject.userid,null,null,null,null,null,null,this.receiveObject);
        console.log('currentBlock :'+currentBlock + ' transactionblock: '+transactionblock);
        if( currentBlock -transactionblock >= sails.config.globals.btc_confirm_block_number ){
            TradeAssetUtil.updateAssetTx(this.receiveObject.tx,sails.config.asset.tx_confirmed);
            return true;
        }else{
            return false;
        }
    }
    
    removeReceiveObjectFromConfirmMQ(id){
        return BatchUtil.removeFromMQ(sails.config.mq.exchange_confirm_mq_btc,id);
    }

    getReceiveObjFromConfrimMQ(){
        return BatchUtil.getFromMQ( sails.config.mq.exchange_confirm_mq_btc ,10)
    }
}

module.exports = ExchangeBTCBlockConfirmation;