let AssetUtil    = require('../utils/AssetUtil');
let CommonUtil   = require('../utils/CommonUtil');

class SendConfirmProcessor{

    constructor(sendObject, mqid, batch) 
    {
      this.sendObject = sendObject;
      this.batch      = batch;
      this.mqid       = mqid;
    }
    
    async execute(){

      var checkParaRes = await this.batch.checkInputData( this.sendObject , this.mqid );
      
      if(  checkParaRes == false || checkParaRes == 'false' )
      {
        try{
            await this.batch.removeSendObjectFromMQ(this.mqid);
          }catch(exception){
            sails.log.error('batch.removeSendObjectFromMQ',exception);
          }
          return reject({ input_data_error : this.sendObject , mqid : this.mqid });
      }

      let txid;
      try{
          txid = await this.batch.sendToAddress( this.sendObject.address , this.sendObject.size );
      }catch(exception){
          sails.log.error('batch.sendToAddress',exception);
      }

      let updateAsset;
      try{
          updateAsset = await this.batch.updateAsset(this.sendObject.userid,this.sendObject.size);
      }catch(exception){
         sails.log.error('batch.updateAsset',exception);
      }

      let updateAssetInRedis; 
      try
      {
          updateAssetInRedis = await this.batch.updateAssetInRedis(this.sendObject.userid,updateAsset);
      }catch(exception){
          sails.log.error('batch.updateAssetInRedis',exception);
      }

      let initAssetHistory;
      try{
          initAssetHistory = await this.batch.initAssetHistory(this.sendObject.userid,this.sendObject.size,txid);
      }catch(exception){
         sails.log.error('batch.initAssetHistory',exception);
      }

     

      let initAssetHistoryRedis 
      try{
          initAssetHistoryRedis= await this.batch.initAssetHistoryInRedis( this.sendObject.userid , initAssetHistory , CommonUtil.getNowFormatDate() );
      }catch(exception){
          sails.log.error('batch.initAssetHistoryInRedis',exception);
      }
      
      try{
          await this.batch.isConfirm(txid);
      }catch(exception){
          sails.log.error('batch.isConfirm',exception);
      }

      let assetHistory;
      try{
          assetHistory = await this.batch.updateAssetHistoryChecked( initAssetHistory.id);
      }catch(exception){
        sails.log.error("batch.updateAssetHistoryChecked",exception);
      }

      

      let assetHistoryRedis;
      try{
          assetHistoryRedis =await this.batch.updateAssetHistoryInRedis( this.sendObject.userid , assetHistory , CommonUtil.getNowFormatDate() );
      }catch(exception){
        sails.log.error("batch.updateAssetHistoryChecked",exception);
      }

      try{
          await this.batch.removeSendObjectFromMQ(this.mqid);
      }catch(exception){
          sails.log.error('batch.removeSendObjectFromMQ',exception);
      }
        
    }
}

module.exports = SendConfirmProcessor;