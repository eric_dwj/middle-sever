let JsonRPCUtil          = require('../utils/JsonRPCUtil');
let TradeAssetUtil       = require('../utils/TradeAssetUtil');
let TradeFinishedOrder   = require('../classes/TradeFinishedOrder');
let TradeOrderDeals      = require('../classes/TradeOrderDeals');
let TradeConfirmOrder    = require('../classes/TradeConfirmOrder');

class TradeMatch{
	
	constructor( userid , assetFrozen , assetPurchase , market , order )
	{
        this.userid         = userid;
        this.assetFrozen    = assetFrozen;
        this.assetPurchase  = assetPurchase;
        this.order          = order;
        this.market         = market;
	}

     processMatch()
    {
       return new Promise(async (resolve,reject)=>{
            try
            {
                var finishedOrderProcessor = new TradeFinishedOrder( this.userid , this.assetFrozen , this.assetPurchase , this.order );           
                await finishedOrderProcessor.processFinishOrder();             
            }
            catch(exception)
            {
              reject(exception);
              sails.log.error(exception);
            }



            try
            {

                

               

                var orderDealsProcessor = new TradeOrderDeals( this.order.id );
                var orderDeals          = await orderDealsProcessor.fetchOrderDeals(0);
                
                for (var i = orderDeals.length - 1; i >= 0; i--) 
                {
                    var confirmOrder = new TradeConfirmOrder( this.market,orderDeals[i].deal_order_id )
                    var orderInfo    = await confirmOrder.getOrder();
                    var finishedOrderProcessor = new TradeFinishedOrder( 
                                            orderInfo.user, 
                                            this.assetPurchase,
                                            this.assetFrozen , 
                                            orderInfo , 
                                            orderDeals[i] 
                                            );
                    await finishedOrderProcessor.processFinishOrder();
                }

                resolve(orderDeals);
            }
            catch(exception)
            {
              reject(exception);
              sails.log.error(exception);
            }

       }); 
    }

	
}

module.exports = TradeMatch;