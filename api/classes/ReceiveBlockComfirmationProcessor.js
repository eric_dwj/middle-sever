
let CommonUtil = require('../utils/CommonUtil');

class ReceiveBlockComfirmationProcessor
{
  constructor(receiveObject,mqid, batch) 
  {
    this.receiveObject = receiveObject;
    this.batch   = batch;
    this.mqid    = mqid;
  }


   async execute(){

    return new Promise(async(resolve,reject)=>{
            var  checkParaRes = await this.batch.checkInputData( this.receiveObject , this.mqid );
            if(  checkParaRes == false || checkParaRes == 'false' )
            {
              try
              {
                await this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
              }
              catch(exception){
                sails.log.error('removeReceiveObjectFromConfirmMQ :mqid ',this.mqid);
                sails.log.error(exception);

              }
              return reject({input_data_error:this.receiveObject,mqid:this.mqid});

            }

            let currentBlock;
            try
            {
              currentBlock = await this.batch.getCurrentBlock();
            }
            catch(exception){
                sails.log.error('batch.getCurrentBlock :currentBlock ',currentBlock);
                sails.log.error(exception);
                return reject(exception); 

            }

           

            var isconfirmed = this.batch.isConfirm( this.receiveObject.blockNumber , currentBlock );
           
            if( ! isconfirmed )
            {
               return resolve(isconfirmed);
            }

            this.receiveObject.assetHistoryDate = CommonUtil.getNowFormatDate();

            let txid; 
            try
            {
                txid = await this.batch.sendToInWallet(
                                                       this.batch.getAccount(this.receiveObject.account)
                                                      ,this.receiveObject.balance
                                                      );
            }
            catch(exception){
                sails.log.error("batch.sendToInWallet",exception);
                this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
                return reject(exception);
            }

            this.receiveObject.txid = txid;

            let initAssetHistory;
            try{
                initAssetHistory = await this.batch.initAssetHistory(this.receiveObject.userid,this.receiveObject.balance,txid);

            }
            catch(exception){
               sails.log.error("batch.initAssetHistory",exception);
               this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
               return reject(exception);
            }

            this.receiveObject.assetHistory = initAssetHistory;
            
            let initAssetHistoryRedis 
            try{
                initAssetHistoryRedis= await this.batch.initAssetHistoryInRedis(this.receiveObject.userid,initAssetHistory,this.receiveObject.assetHistoryDate);
            }
            catch(exception){
                sails.log.error("batch.initAssetHistoryInRedis",exception);
                this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
                return reject(exception);
            }

           
            let updateAsset;
            try{
                updateAsset = await this.batch.updateAsset(this.receiveObject.userid,this.receiveObject.balance);

            }
            catch(exception){
               sails.log.error("batch.updateAsset",exception);
               this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
               return reject(exception);
            }

            let updateAssetInRedis 
            try
            {
                updateAssetInRedis = await this.batch.updateAssetInRedis(this.receiveObject.userid,updateAsset);
            }
            catch(exception){
                sails.log.error("batch.updateAssetInRedis",exception);
                this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
                return reject(exception);
            }
  
            try
            {
               await this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
            }
            catch(exception){
                sails.log.error("batch.removeReceiveObjectFromConfirmMQ",exception);
                this.batch.removeReceiveObjectFromConfirmMQ(this.mqid);
                return reject(exception);
            }

            let inWalletMQID
            try
            {
              inWalletMQID = await this.batch.addReceiveObjectToInWalletMQ(this.receiveObject);
            }
            catch(exception){
                sails.log.error("batch.addReceiveObjectToInWalletMQ",exception);
                return reject(exception);
            }
            resolve( inWalletMQID );

    });
  }

}

module.exports = ReceiveBlockComfirmationProcessor;