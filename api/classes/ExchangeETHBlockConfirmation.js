let TradeAssetUtil     = require('../utils/TradeAssetUtil');
let TradeUpdateBalance = require('../classes/TradeUpdateBalance');
let ETHService         = require('../services/ETHService');
let BatchUtil          = require('../utils/BatchUtil');
class ExchangeETHBlockConfirmation{
    constructor(){
       ExchangeETHBlockConfirmation.instance = this;
       this.receiveObject = null;
    }



    async checkInputData(receiveObject,mqid){
      console.log('checkInputData',receiveObject);
      if( !mqid )
      {
        return false;
      }


      if( !receiveObject.timestamp )
      {
        return false;
      }
      
      if( !receiveObject.userid )
      {
        return false;
      }

      if( !receiveObject.assetname )
      {
        return false;
      }
    
      if( !receiveObject.size )
      {
         return false;
      }
      
      if( !receiveObject.detail )
      {
         return false;
      }

      if( !receiveObject.blockNumber )
      {
         return false;
      }

      if( !receiveObject.tx )
      {
         return false;
      }

      this.receiveObject = receiveObject;

      return true;

    }
    
    getCurrentBlock()
    {
        return ETHService.getCurrentBlock();
    }
    
    updateBalance(receiveObject)
    {
        var    updateBalanceProcessor = new TradeUpdateBalance(receiveObject.userid,receiveObject.assetname,sails.config.trader.business_deposit,receiveObject.size,receiveObject.detail);
        return updateBalanceProcessor.updateBalance();
    }

    isConfirm(transactionblock,currentBlock)
    {
      sails.log.debug('transactionblock:'+transactionblock+'  currentBlock:'+currentBlock+' confirm:'+(currentBlock-transactionblock));
      this.receiveObject.confirmedBlock     = currentBlock-transactionblock;
      this.receiveObject.confirmBlockNumber = sails.config.globals.eth_confirm_block_number;
      TradeAssetUtil.addChangedInfoToMq(this.receiveObject.userid,null,null,null,null,null,null,this.receiveObject);

        if( currentBlock -transactionblock >= sails.config.globals.eth_confirm_block_number )
        {
            TradeAssetUtil.updateAssetTx(this.receiveObject.tx,sails.config.asset.tx_confirmed);
            return true;
        }else
        {
            return false;
        }
    }
    
    removeReceiveObjectFromConfirmMQ(id)
    {
      
        return BatchUtil.removeFromMQ(sails.config.mq.exchange_confirm_mq_eth,id);
    }

    getReceiveObjFromConfrimMQ()
    {
        return BatchUtil.getFromMQ( sails.config.mq.exchange_confirm_mq_eth ,10)
    }
}

module.exports = ExchangeETHBlockConfirmation;