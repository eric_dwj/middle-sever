let BatchUtil  = require('../utils/BatchUtil');
let TokenUtil    = require('../utils/TokenUtil');
let AssetUtil  = require('../utils/AssetUtil');
let CommonUtil = require('../utils/CommonUtil');

class TokenBatchReceiveByAccount{
    constructor() {
      TokenBatchReceiveByAccount.instance = this;
      this.index = 0;
      this.receiveObject = null;
    }

    getAddressFromAccount(account)
    {
        var assetAccountName = String(this.receiveObject.assetname).toUpperCase() + 'Account';
        return account[assetAccountName].address;
    }

    async checkInputData(receiveObject,mqid)
    {
      this.receiveObject = receiveObject;

      if( !mqid )
      {
        return false;
      }

      if( !receiveObject.timestamp )
      {
        return false;
      }

      if( !receiveObject.userid )
      {
        return false;
      }

      if( !receiveObject.assetname )
      {
        return false;
      }

      if( !receiveObject.account )
      {
        return false;
      }

      var assetAccountName = String(this.receiveObject.assetname).toUpperCase() + 'Account';

      if( !receiveObject.account[assetAccountName] )
      {
        return false;
      }

      let validateRes;
      validateRes = await TokenUtil.validateaddress(receiveObject.account[assetAccountName].address)
      
      if( !validateRes )
      {
        return false;
      }

      
    
      
      return true;

    }

    getCurrentBlock()
    {
        return TokenService.getCurrentBlock();
    }

    markNotInProcess(account){
      var assetAccountName = String(this.receiveObject.assetname).toUpperCase() + 'Account';
      return BatchUtil.markAccountNotInProcess(account[assetAccountName].address);
    }

    isOvertime(timestamp){
       if(sails.config.mq.on_receive_ttl < new Date().getTime() -parseInt(timestamp))
       {
        return true
       }else
       {
        return false;
       }
    }

     getReceiveObjectFromReceiveMQ()
    {
       var receiveMqName = 'receive_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
       // var confirmMQName = 'exchange_send_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
        this.index++;
        if( this.index ==sails.config.mq.tokens.length )
        {
          this.index = 0;
        }
       return BatchUtil.getFromMQ( sails.config.mq[receiveMqName] ,10)
    }

    getBalance(address,userid) 
    {
        return new Promise(async (resolve,reject)=>{
           let balance;
           try{
                balance = await TokenService.balance(  address , this.receiveObject.assetname )
           }catch(exception)
           {
            sails.log.error('error :address:'+ address +' userid:'+userid, exception);
           }
           sails.log.debug("balance : "+balance+" address: "+address);
          return resolve(balance);
          
        }) 
    }

    checkBalance(balance) 
    {  
        var assetsName       = 'assets_' + String( this.receiveObject.assetname ).toLowerCase() + '_name';
        var receiveThreshold = 'token_'  + String( this.receiveObject.assetname ).toLowerCase() + '_receive_threshold';

         if( !balance )
         {
          return false;
         }
         else if( sails.config.asset[assetsName] && balance < sails.config.globals[receiveThreshold] )
         {   
            return true;
         }
         else
         {
            return false;
         }
    }

    addReceiveObjectToConfirmMQ(receiveObject)
    {
        var confirmMqName = 'confirm_mq_'+String( this.receiveObject.assetname ).toLowerCase(); 
        return BatchUtil.putToMQ(sails.config.mq[confirmMqName],JSON.stringify(receiveObject));
    }
    
   removeReceiveObjectFromReceiveMQ(id)
   {
        var receiveMqName = 'receive_mq_'+String( this.receiveObject.assetname ).toLowerCase();
        return BatchUtil.removeFromMQ(sails.config.mq[receiveMqName],id);
   }

};

module.exports = TokenBatchReceiveByAccount;
