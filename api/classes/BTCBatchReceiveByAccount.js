let BatchUtil  = require('../utils/BatchUtil');
let BTCUtil    = require('../utils/BTCUtil');
let AssetUtil  = require('../utils/AssetUtil');
let CommonUtil = require('../utils/CommonUtil');

class BTCBatchReceiveByAccount{
    constructor() {
      BTCBatchReceiveByAccount.instance = this;
    }

    getAddressFromAccount(account)
    {
      return account.BTCAccount.address;
    }

    async checkInputData(receiveObject,mqid)
    {

      if( !mqid )
      {
        return false;
      }

      if( !receiveObject.timestamp )
      {
        return false;
      }

      if( !receiveObject.userid )
      {
        return false;
      }

      if( !receiveObject.assetname )
      {
        return false;
      }

      if( !receiveObject.account )
      {
        return false;
      }

      let validateRes;

      validateRes = await BTCUtil.validateaddress(receiveObject.account.BTCAccount.address)
      
      if( !validateRes )
      {
        return false;
      }
    

      return true;

    }

    getCurrentBlock()
    {
        return BTCService.getCurrentBlock();
    }

    markNotInProcess(account){
      return BatchUtil.markAccountNotInProcess(account.BTCAccount.address);
    }

    isOvertime(timestamp){

      if(sails.config.mq.on_receive_ttl < new Date().getTime() -parseInt(timestamp)){
          return true
      }else{
          return false;
      }
    }    

    getReceiveObjectFromReceiveMQ()
    {
       return BatchUtil.getFromMQ( sails.config.mq.receive_mq_btc ,10)
    } 

    getBalance(address,userid) 
    {
        return new Promise((resolve,reject)=>{
          BTCService.balance( address ).then((balance)=>{
              sails.log.debug('balance :'+balance+' address: '+address);
              return resolve(balance);
          });
        }) 
    }

    checkBalance(balance) 
    {     
         if( !balance )
         {
          return false;
         }
        
         if( balance < sails.config.globals.btc_receive_threshold )
         {
             
            return true;
         }
         else
         {
            return false;
         }
    }

    addReceiveObjectToConfirmMQ(receiveObject)
    {
        return BatchUtil.putToMQ(sails.config.mq.confirm_mq_btc,JSON.stringify(receiveObject));

    }
   removeReceiveObjectFromReceiveMQ(id)
   {
        return BatchUtil.removeFromMQ(sails.config.mq.receive_mq_btc,id);
   }

};

module.exports = BTCBatchReceiveByAccount;
