let BatchUtil  = require('../utils/BatchUtil');
let ETHUtil    = require('../utils/ETHUtil');
let AssetUtil  = require('../utils/AssetUtil');
let CommonUtil = require('../utils/CommonUtil');


class ETHBatchReceiveByInWallet{

    constructor() {
        ETHBatchReceiveByInWallet.instance = this;
    }

    async checkInputData(receiveObject,mqid)
    {

      if( !mqid )
      {
        return false;
      }

      if( !receiveObject.timestamp )
      {
        return false;
      }

      if( !receiveObject.assetHistory )
      {
        return false;
      }

      if( !receiveObject.assetHistoryDate )
      {
        return false;
      }

      if( !receiveObject.userid )
      {
        return false;
      }

      if( !receiveObject.assetname )
      {
        return false;
      }

      if( !receiveObject.txid )
      {
        return false;
      }

      if( !receiveObject.account )
      {
        return false;
      }
      if( !receiveObject.balance )
      {
         return false;
      }


      if( !receiveObject.blockNumber )
      {
         return false;
      }

      let validateRes;

      validateRes = await ETHUtil.validateaddress(receiveObject.account.ETHAccount.address)
      
      if( !validateRes )
      {
        return false;
      }

      return true;

    }

    getReceiveObjectFromInWalletMQ()
    {
        return BatchUtil.getFromMQ( sails.config.mq.inwallet_mq_eth ,10)
    }

    isConfirmed(txid){
      return new Promise(async (resolve,reject)=>{
          var trans = await ETHUtil.getTransactionReceipt(txid);
          if( trans)
          {
            resolve(true);
          }
          else
          {
            resolve(false);
          }
      });
    
    }

    updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate){
        return AssetUtil.updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate,sails.config.asset.assets_history_state_deposited_checked);
    }

    updateAssetHistoryChecked(id){
        return AssetUtil.updateAssetHistory(id,sails.config.asset.assets_history_state_deposited_checked);
    }

    removeReceiveObjectFromInWalletMQ(id)
    {
        return BatchUtil.removeFromMQ( sails.config.mq.inwallet_mq_eth,id )
    }


}

module.exports = ETHBatchReceiveByInWallet;