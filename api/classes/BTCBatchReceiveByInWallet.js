let BatchUtil  = require('../utils/BatchUtil');
let BTCUtil    = require('../utils/BTCUtil');
let AssetUtil  = require('../utils/AssetUtil');
let CommonUtil = require('../utils/CommonUtil');

class BTCBatchReceiveByInWallet{
     constructor() {
      BTCBatchReceiveByInWallet.instance = this;
     }


    async checkInputData(receiveObject,mqid)
    {
      // sails.log.debug("##this.checkInputData##",mqid);

      if( !mqid )
      {
        return false;
      }



      // sails.log.debug("##this.checkInputData##",receiveObject.timestamp );

      if( !receiveObject.timestamp )
      {
        return false;
      }

      if( !receiveObject.assetHistory)
      {
        return false;
      }

      if( !receiveObject.assetHistoryDate)
      {
        return false;
      }


      
      // sails.log.debug("##this.checkInputData##",receiveObject.userid);
      if( !receiveObject.userid )
      {
        return false;
      }

       // sails.log.debug("##this.checkInputData##",receiveObject.assetname);

      if( !receiveObject.assetname )
      {
        return false;
      }
      // sails.log.debug("##this.checkInputData##1");

      if( !receiveObject.txid )
      {
        return false;
      }

      if( !receiveObject.account )
      {
        return false;
      }
      // sails.log.debug("##this.checkInputData##2");
      if( !receiveObject.balance )
      {
         return false;
      }

      // sails.log.debug("##this.checkInputData##3");

      if( !receiveObject.blockNumber ){
         return false;
      }

      let validateRes;

      validateRes = await BTCUtil.validateaddress(receiveObject.account.BTCAccount.address)

      if( !validateRes ){
        return false;
      }

      return true;
    }

    getReceiveObjectFromInWalletMQ()
    {
        return BatchUtil.getFromMQ( sails.config.mq.inwallet_mq_btc ,10)
    }

    isConfirmed(txid){
      return new Promise(async (resolve,reject)=>{
          var trans = await BTCUtil.getrawtransaction(txid);
          if( trans && trans.confirmations && trans.confirmations > 0 )
          {
            resolve(true);
          }
          else
          {
            resolve(false);
          }
      });
    
    }

    updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate){
        return AssetUtil.updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate,sails.config.asset.assets_history_state_deposited_checked);
    }

    updateAssetHistoryChecked(id){
        return AssetUtil.updateAssetHistory(id,sails.config.asset.assets_history_state_deposited_checked);
    }

    removeReceiveObjectFromInWalletMQ(id)
    {
        return BatchUtil.removeFromMQ( sails.config.mq.inwallet_mq_btc,id )
    }
}

module.exports = BTCBatchReceiveByInWallet;