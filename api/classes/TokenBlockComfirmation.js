let BatchUtil  = require('../utils/BatchUtil');
let TokenUtil    = require('../utils/TokenUtil');
let AssetUtil  = require('../utils/AssetUtil');
let CommonUtil = require('../utils/CommonUtil');

class TokenBlockComfirmation{
    constructor() {
      TokenBlockComfirmation.instance = this;
      this.receiveObject = null;
      this.index         = 0;
    }

     
    getAccount(account)
    {
        var assetAccountName = String(this.receiveObject.assetname).toUpperCase() + 'Account';
        return account[assetAccountName];
    }

    getReceiveObjFromConfrimMQ()
    {
        var confirmMQName = 'confirm_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
        // var confirmMQName = 'exchange_send_mq_'+String( sails.config.mq.tokens[this.index] ).toLowerCase();
        this.index++;
        if( this.index ==sails.config.mq.tokens.length )
        {
          this.index = 0;
        }
        return BatchUtil.getFromMQ( sails.config.mq[confirmMQName] ,10)
    }


    async checkInputData(receiveObject,mqid)
    {
      this.receiveObject = receiveObject;

      if( !mqid )
      {
        return false;
      }

      if( !receiveObject.timestamp )
      {
        return false;
      }

      if( !receiveObject.userid )
      {
        return false;
      }

      if( !receiveObject.assetname )
      {
        return false;
      }
      
      if( !receiveObject.account )
      {
        return false;
      }
      
      if( !receiveObject.balance )
      {
         return false;
      }

      if( !receiveObject.blockNumber )
      {
         return false;
      }

      var assetAccountName = String( receiveObject.assetname ).toUpperCase() + 'Account';
      
      if( !receiveObject.account[assetAccountName] )
      {
        return false;
      }

      let validateRes;
      
      validateRes = await TokenUtil.validateaddress(receiveObject.account[assetAccountName].address)
      
      if( !validateRes )
      {
        return false;
      }

      

      return true;

    }

    getCurrentBlock()
    {
        return TokenService.getCurrentBlock();
    }


    isConfirm(transactionblock,currentBlock)
    {
      sails.log.debug('transactionblock:'+transactionblock+'  currentBlock:'+currentBlock+' confirm:'+(currentBlock-transactionblock));
      
        if( currentBlock -transactionblock >= sails.config.globals.token_confirm_block_number )
        {
            return true;
        }else
        {
            return false;
        }
    }


     initAssetHistory(userid,balance,txid)
    {
       return AssetUtil.initAssetHistoryUnchecked(
        userid,
        this.receiveObject.assetname,
        balance,
        txid,
        sails.config.asset.assets_history_side_deposit,
        sails.config.asset.assets_history_state_deposited_unchecked
        );
    }

    updateAsset(userid,balance)
    {
      var asset = {};
      var assetAvailableName       = String(this.receiveObject.assetname).toLowerCase() + 'Available';
      
      asset.userid                 = userid;
      asset[assetAvailableName]    = balance;
      return AssetUtil.updateAssetReceive( userid , asset ,assetAvailableName );
    }

    initAssetHistoryInRedis(userid,assetHistory,assetHistoryDate)
    {
        return AssetUtil.updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate,sails.config.asset.assets_history_state_deposited_unchecked);
    }

    updateAssetInRedis(userid,asset)
    {
        return AssetUtil.updateAssetInRedis(userid,asset);
    }


    removeReceiveObjectFromConfirmMQ(id)
    {
        var confirmMQName = 'confirm_mq_'+String( this.receiveObject.assetname ).toLowerCase();
        return BatchUtil.removeFromMQ(sails.config.mq[confirmMQName],id);
    }
    addReceiveObjectToInWalletMQ(receiveObject)
    {
        var inwalletMQName = 'inwallet_mq_'+String( this.receiveObject.assetname ).toLowerCase();
        return BatchUtil.putToMQ(sails.config.mq[inwalletMQName] ,JSON.stringify(receiveObject));
    }


    sendToInWallet(account,balance)
    {
        var gasPrice       = TokenUtil.getReceiveGasPriceByGwei();
        var pk             = account.privateKey;
        var to             = sails.config.globals.token_in_address; 
        var amount         = balance;
      
        let result;
        try
        {

            result = TokenService.transferWithBalanceCheck(to,pk,gasPrice,amount,this.receiveObject.assetname);
        }catch(exception){
            sails.log.error('TokenService.transferWithBalanceCheck account:'+account +' balance: '+balance);
            sails.log.error(exception);
        }

        return result;
    }

}

module.exports = TokenBlockComfirmation;
