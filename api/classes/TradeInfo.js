let JsonRPCUtil     = require('../utils/JsonRPCUtil');
let TradeAssetUtil  = require('../utils/TradeAssetUtil');

class TradeInfo{
	
	constructor( userid )
	{
		this.userid = userid;
	}

    getOrder()
    {

        return new Promise( async (resolve,reject)=>{

            let OrderFromRedis;
            
            try
            {
                OrderFromRedis = await TradeAssetUtil.getOrderInRedis(this.userid);
                
            }catch(exception)
            {
                sails.log.error(exception);
            }

            resolve(OrderFromRedis);
        });   

    }

    getAssetHistory()
    {
        return new Promise( async (resolve,reject)=>{
                let assetHistoryFromRedis;
                
                try
                {
                    assetHistoryFromRedis = await TradeAssetUtil.getTradeAssetHistoryInRedis(this.userid);
                }catch(exception)
                {
                    sails.log.error(exception);
                }

                resolve(assetHistoryFromRedis);

        });   
    }

	getBalance()
    {
        
        return new Promise( async (resolve,reject)=>{
                let balanceFromRedis;
                
                try
                {
                    balanceFromRedis = await TradeAssetUtil.getTradeAssetInRedis(this.userid);
                }
                catch(exception)
                {
                    sails.log.error(exception);
                }

                resolve(JSON.parse(balanceFromRedis));

        });     
    }
}

module.exports = TradeInfo;