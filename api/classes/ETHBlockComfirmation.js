let ETHUtil    = require('../utils/ETHUtil');
let AssetUtil  = require('../utils/AssetUtil');
let CommonUtil = require('../utils/CommonUtil');
let BatchUtil  = require('../utils/BatchUtil');


class ETHBlockComfirmation{
    constructor() {
  	   ETHBlockComfirmation.instance = this;
    }

    getAccount(account)
    {
        return account.ETHAccount;
    }

    getReceiveObjFromConfrimMQ()
    {
        return BatchUtil.getFromMQ( sails.config.mq.confirm_mq_eth ,10)
    }


    async checkInputData(receiveObject,mqid)
    {

      if( !mqid )
      {
        return false;
      }


      if( !receiveObject.timestamp )
      {
        return false;
      }
      
      if( !receiveObject.userid )
      {
        return false;
      }

      if( !receiveObject.assetname )
      {
        return false;
      }

      if( !receiveObject.account )
      {
        return false;
      }
    
      if( !receiveObject.balance )
      {
         return false;
      }

      if( !receiveObject.blockNumber )
      {
         return false;
      }

      let validateRes;

      validateRes = await ETHUtil.validateaddress(receiveObject.account.ETHAccount.address)
      
      if( !validateRes )
      {
        return false;
      }

      return true;

    }

    getCurrentBlock()
    {
        return ETHService.getCurrentBlock();
    }


    isConfirm(transactionblock,currentBlock)
    {
      sails.log.debug('transactionblock:'+transactionblock+'  currentBlock:'+currentBlock+' confirm:'+(currentBlock-transactionblock));
      
        if( currentBlock -transactionblock >= sails.config.globals.eth_confirm_block_number )
        {
            return true;
        }else
        {
            return false;
        }
    }


     initAssetHistory(userid,balance,txid)
    {
       return AssetUtil.initAssetHistoryUnchecked(
        userid,
        sails.config.asset.assets_eth_name,
        balance,
        txid,
        sails.config.asset.assets_history_side_deposit,
        sails.config.asset.assets_history_state_deposited_unchecked
        );
    }

    updateAsset(userid,balance)
    {
      var asset = {};
      asset.userid          = userid;
      asset.ethAvailable    = balance;
      return AssetUtil.updateAssetReceive( userid , asset ,'ethAvailable');
    }

    initAssetHistoryInRedis(userid,assetHistory,assetHistoryDate)
    {
        return AssetUtil.updateAssetHistoryInRedis(userid,assetHistory,assetHistoryDate,sails.config.asset.assets_history_state_deposited_unchecked);
    }

    updateAssetInRedis(userid,asset)
    {
        return AssetUtil.updateAssetInRedis(userid,asset);
    }


    removeReceiveObjectFromConfirmMQ(id)
    {
      
        return BatchUtil.removeFromMQ(sails.config.mq.confirm_mq_eth,id);
    }
    addReceiveObjectToInWalletMQ(receiveObject)
    {
        return BatchUtil.putToMQ(sails.config.mq.inwallet_mq_eth,JSON.stringify(receiveObject));
    }


    sendToInWallet(account,balance)
    {
        var gasPrice       = ETHUtil.getReceiveGasPriceByGwei();
        var pk             = account.privateKey;
        var to             = sails.config.globals.eth_in_address; 
        var amount         = balance- sails.config.globals.eth_receive_transferfee;
      
        let result;
        try
        {
            result = ETHService.transferWithBalanceCheck(to,pk,gasPrice,amount);
        }catch(exception){
            sails.log.debug('ETHService.transferWithBalanceCheck account:'+account +' balance: '+balance);
            sails.log.debug(exception);
        }

        return result;
    }

}

module.exports = ETHBlockComfirmation;

