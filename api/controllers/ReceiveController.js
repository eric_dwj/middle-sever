/**
 * ReceiveController
 *
 * @description :: Server-side logic for managing receives
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function(req, res) {
		
	     if(! req.body.userid)
	     {
	     	return res.json(200, {err: 'userid required'}); 
	     }

	       if(! req.body.assetname)
	     {
	     	return res.json(200, {err: 'assetname required'}); 
	     }


	      UserService.getAccountByUserid(req.body.userid)
	      .then((account)=>{

	      	  if(account)
	      	  {
	      	  	 var receiveObject = 
              {
              	userid:req.body.userid,
              	account:account,
              	timestamp:new Date().getTime(),
              	assetname:req.body.assetname
              };

              ReceiveService.addReceiveObjectToReceiveMQ(receiveObject);

              res.json({account:account});

	      	  }else
	      	  {
	      	  	 res.json({error:"account_not_found"});
	      	  }
	      });
	}
	
};

