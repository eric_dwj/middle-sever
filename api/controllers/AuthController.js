
module.exports = {
  index: function (req, res) {
  
    let author = req.body;

    if (!author.userid || !author.password) {
      return res.json(401, {err: 'email and password required'});
    }

    Users.findOne({userid: author.userid}, function (err, user) {
      if (!user) {
        return res.json(401, {err: 'invalid email or password'});
      }

      Users.comparePassword(author.password, user, function (err, valid) {
        if (err) {
          return res.json(403, {err: 'forbidden'});
        }

        if (!valid) {
          return res.json(401, {err: 'invalid email or password'});
        } else {
          res.json({
            user: user,
            token: jwToken.issue({id : user.userid })
          });
        }
      });
    })
  }
};
