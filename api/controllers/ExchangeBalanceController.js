/**
 * ExchangeBalanceController
 *
 * @description :: Server-side logic for managing exchangebalances
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	asset_tx:function(req,res){
		if( !req.body.userid ){
            return res.json(200, {err: 'userid required'}); 
        }
        
        try{
        	TradeService.fetchAssetTx(req.body.userid).then((records)=>{
        	res.send(records);
        })

        }catch(exception){
        	res.json(exception);
        }
	},

    withdraw:function(req,res){
        if( !req.body.userid ){
            return res.json(200, {err: 'userid required'}); 
        }

        if( !req.body.assetname ){
            return res.json(200, {err: 'assetname required'}); 
        }

        if( !req.body.address ){
            return res.json(200, {err: 'address required'}); 
        }

        if( !req.body.detail ){
            return res.json(200, {err: 'detail required'}); 
        }

        if( !req.body.size ){
            return res.json(200, {err: 'size required'}); 
		}
		
		var sendObject = {};
		sendObject.timestamp = new Date().getTime();
		sendObject.size      = req.body.size;
		sendObject.detail    = req.body.detail;
		sendObject.address   = req.body.address;
		sendObject.assetname = req.body.assetname;
		sendObject.userid    = req.body.userid;

		ExchangeSendService.addSendObjectToSendMQ(sendObject)
		.then((result)=>{
			return res.json(200, {msg: 'submit_success',result:result}); 
		}).catch((exception)=>{
			return res.json(200, {error:exception}); 
		})


	},
	deposit: function(req, res) {

		if( !req.body.size ){
			return res.json(200, {err: 'size required'}); 
		}

		if( !req.body.detail ){
			return res.json(200, {err: 'detail required'}); 
		}
		
		if( !req.body.userid ){
			return res.json(200, {err: 'userid required'}); 
		}

		if( !req.body.assetname ){
			return res.json(200, {err: 'assetname required'}); 
		}


		if( req.body.assetname == sails.config.asset.assets_btc_name ){
			if(!req.body.pk){
				return res.json(200, {err: 'pk required'}); 
			}

			if(!req.body.transactionFee){
				return res.json(200, {err: 'transactionFee required'}); 
			}

			TransferService
			.btc(
				sails.config.globals.btc_in_address,
				req.body.size,
				req.body.pk,
				req.body.transactionFee,
				)
			.then((tx)=>{
				var receiveObject = {};
				receiveObject.timestamp = new Date().getTime();
				receiveObject.assetname = req.body.assetname;
				receiveObject.userid    = req.body.userid;
				receiveObject.detail    = req.body.detail;
				receiveObject.size      = req.body.size;
				receiveObject.tx        = tx;

				ExchangeReceiveService
				.addReceiveObjectToReceiveMQ(receiveObject)
				.then(resp=>{
					return res.json(200,{resp:resp,tx:tx});
				});
			}).catch((exception)=>{
				return res.json(200, {error: exception});
			});
		}else if( req.body.assetname == sails.config.asset.assets_eth_name ){
			if(!req.body.pk){
				return res.json(200, {err: 'pk required'}); 
			}

			if(!req.body.gasprice){
				return res.json(200, {err: 'gasprice required'}); 
			}

			TransferService
			.eth(
				sails.config.globals.eth_in_address,
				req.body.pk,
				req.body.gasprice,
				req.body.size
				)
			.then((tx)=>{
				var receiveObject = {};
				receiveObject.timestamp = new Date().getTime();
				receiveObject.assetname = req.body.assetname;
				receiveObject.userid    = req.body.userid;
				receiveObject.detail    = req.body.detail;
				receiveObject.size      = req.body.size;
				receiveObject.tx        = tx;

				ExchangeReceiveService
				.addReceiveObjectToReceiveMQ(receiveObject)
				.then(resp=>{
					return res.json(200,{resp:resp,tx:tx});
				});
			}).catch((exception)=>{
				return res.json(200, {error: exception});
			});
		}else{
			if(!req.body.pk){
				return res.json(200, {err: 'pk required'}); 
			}

			if(!req.body.gasprice){
				return res.json(200, {err: 'gasprice required'}); 
			}

			TransferService
			.token(
				sails.config.globals.token_in_address,
				req.body.pk,
				req.body.gasprice,
				req.body.size,
				req.body.assetname
				)
			.then((tx)=>{
				var receiveObject = {};
				receiveObject.timestamp = new Date().getTime();
				receiveObject.assetname = req.body.assetname;
				receiveObject.userid    = req.body.userid;
				receiveObject.detail    = req.body.detail;
				receiveObject.size      = req.body.size;
				receiveObject.tx        = tx;

				ExchangeReceiveService
				.addReceiveObjectToReceiveMQ(receiveObject)
				.then(resp=>{
					return res.json(200,{resp:resp,tx:tx});
				});
			}).catch((exception)=>{
				return res.json(200, {error: exception});
			});
		}

		

		

		

		


	}
	
};

