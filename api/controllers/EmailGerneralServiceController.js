/**
 * EmailGerneralServiceController
 *  通用邮件服务，通过邮件模版（html模版），可以置换四个参数，并且把邮件发送出去
 *  这个API和MailService一起结合使用
 */

module.exports = {
	create: function(req, res){
	   var mailData = req.body;


	    MailService
	    .generateMailHtml(
	    					mailData.metadataCode,
	    	                mailData.value1,
	    	                mailData.value2,
	    	                mailData.value3,
	    	                mailData.value4
	    	              )
	    .then((html)=>{

	   	 var mailOptions = {
								from: mailData.from,
								  to: mailData.to,
							 subject: mailData.subject,
								html: html
						    };
        console.log(mailOptions);
        MailService
        .sendMail(mailOptions)
        .then((info)=>{
	        res.json({emailinfo:info});
        });

	   }).catch((err)=>{
	   	 console.log(err);
	   })
	}
	
};

