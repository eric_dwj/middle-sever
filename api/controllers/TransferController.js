/**
 * TransferController
 *
 * @description :: Server-side logic for managing transfers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	getTicker:function(req,res){
		TransferService.getTicker()
		.then((resp)=>{
			 
			res.send(resp);
		})
	},

    qrcode:function (req, res) {
	     if(! req.body.address)
	     {
	     	return res.json(200, {err: 'address required'}); 
	     }

		TransferService.qrcode(req.body.address)
		.then((url)=>{
			res.json(200, {img: url}); 
		})
	},
    
    btcbalance:function(req,res){
    	if( ! req.body.address )
		{
			return res.json(200, {err: 'address required'}); 
		}

		BalanceService
		.btc(req.body.address)
		.then((balance)=>{
			res.json(200, {balance:balance} );
		})
    },

    tokenbalance:function(req,res){
    	if( ! req.body.address )
		{
			return res.json(200, {err: 'address required'}); 
		}

    	if( ! req.body.tokenName )
		{
			return res.json(200, {err: 'tokenName required'}); 
		}
		

		BalanceService
		.token( req.body.address , req.body.tokenName )
		.then((balance)=>{
			res.json(200, {balance:balance});
		})


    },

   

    ethbalance:function(req,res){
    	if( ! req.body.address )
		{
			return res.json(200, {err: 'address required'}); 
		}

		BalanceService
		.eth(req.body.address)
		.then((balance)=>{
			res.json(200, {balance:balance});
		})


    },



    btctransaction:function(req,res){
    	if( ! req.body.txid )
		{
			return res.json(200, {err: 'txid required'}); 
		}

		TransferService
		.btcTransaction( req.body.txid )
		.then((data)=>{
           return res.json(200, {data: data}); 
		})

    },
    
    btctransfer:function (req, res) {

    	if( ! req.body.toaddress )
		{
			return res.json(200, {err: 'toaddress required'}); 
		}

		if( ! req.body.pk )
		{
			return res.json(200, {err: 'pk required'}); 
		}

		if( ! req.body.amounttosend )
		{
			return res.json(200, {err: 'amounttosend required'}); 
		}

		if( ! req.body.transactionfee )
		{
			return res.json(200, {err: 'transactionfee required'}); 
		}

       console.log(req.body);

        //toaddress,amountToSend,pk,transactionFee
		TransferService
		.btc( req.body.toaddress , 
			req.body.amounttosend , 
			req.body.pk ,
			req.body.transactionfee )
		.then((hash)=>{
           return res.json(200, {hash: hash}); 

		})
		.catch((error)=>{
			return res.json(200,error);
		})

    },
    //to,pk,gasPrice,amount,tokenName
    tokentransfer:function (req, res) {

    	if( !req.body.to )
		{
			return res.json(200, {err: 'to required'}); 
		}

		if(!req.body.pk)
		{
			return res.json(200, {err: 'pk required'}); 
		}

		if(!req.body.gasprice)
		{
			return res.json(200, {err: 'gasprice required'}); 
		}

		if(!req.body.amount)
		{
			return res.json(200, {err: 'amount required'}); 
		}

		if(!req.body.tokenName)
		{
			return res.json(200, {err: 'tokenName required'}); 
		}

		TransferService
		.token(
			req.body.to,
			req.body.pk,
			req.body.gasprice,
			req.body.amount,
			req.body.tokenName
			)
		.then((txid)=>{
			 return res.json(200, {txid: txid});
		});

    },

	ethtransfer:function (req, res) {


		if( !req.body.to )
		{
			return res.json(200, {err: 'to required'}); 
		}

		if(!req.body.pk)
		{
			return res.json(200, {err: 'pk required'}); 
		}

		if(!req.body.gasprice)
		{
			return res.json(200, {err: 'gasprice required'}); 
		}

		if(!req.body.amount)
		{
			return res.json(200, {err: 'amount required'}); 
		}

		TransferService
		.eth(
			req.body.to,
			req.body.pk,
			req.body.gasprice,
			req.body.amount)
		.then((txid)=>{
			 return res.json(200, {txid: txid});
		})
		.catch((exception)=>{
			return res.json(200, {error: exception});
		})
		;
	}
	
};

