/*
 注册方法: 
 A:通过GET方法：bip39accounts获取助记词，根私钥，各种账号；
 B:通过create方法，完成注册；
     1）userid是用户email；   
     2）password和repeatPassword；  
     3）address是删除了各种privatekey的bip39accounts的返回数据； 
 C:通过importbip39accountsbymnemonic方法导入钱包；
 D:通过importbip39accountsbyseedhex方法导入钱包； 

 注册流程：
 step1:获取用户bip39账号；
 step2:前端用户通过调用encryptWallet加密，完成bip39账号的备份；
 step3:提交注册，注册的同时生成用户的内部充值账号；   
 */
let Util = require('../utils/JsonRPCUtil');
var create_batch;
function createBatchFn(){
  create_batch++;
  console.log("createBatchFn",create_batch);
  if( create_batch >= 4)
  {
    return;
  }
   var data = {};
   data.userid         = new Date().getTime();
   data.password       = "test";
   data.repeatPassword = "test";
   Util.Post(data,"http://localhost:1337/users")
   .then((result)=>{
     createBatchFn();
   });

}
module.exports = {

  createBatch:function(req,res){
    create_batch=0;
    createBatchFn();
    res.ok();
  },
  create: function (req, res) {
     
    var userobj = req.body;

    if (req.body.password !== req.body.repeatPassword) {
      return res.json(200, {err: 'Password doesn\'t match'});
    }

    UserService.register(userobj).then((user)=>{
      res.json(200, {user: user, token: jwToken.issue({id: user.userid})});
    }).catch((err)=>{
      return res.json(err.status, {err: err});
    });

  },
  
  bip39accounts:function (req, res) {
    var bip39info = AccountService.generateAccount();
    res.json(bip39info);
  },
  importbip39accountsbymnemonic:async (req, res)=> {
    
    let userobj = req.body;

    if(!req.body.mnemonic){
       return res.json(200, {err: 'mnemonic required'}); 
    }


    let user = await UserService.find(req.body.mnemonic);
    
    if( user )
    {
      user.wallet = AccountService.impoartAccountByMnemonic( req.body.mnemonic );
      res.json(200, {user: user, token: jwToken.issue({id: user.userid})});
    }
    else
    {
        if (!req.body.password) {
            return res.json(200, {err: 'No user is found with the given mnemonic.'});
        }
        if(req.body.password !== req.body.repeatPassword){
            return res.json(200, {err: 'Password doesn\'t match.'});

        }
      try {
        user        = await UserService.register(userobj);
        user.wallet = AccountService.impoartAccountByMnemonic( req.body.mnemonic );
        res.json(200, {user: user, token: jwToken.issue({id: user.userid})});
      } 
      catch (error) 
      {
        return res.json({err: error});
      }
    }
  },
  importbip39accountsbyseedhex:function (req, res) {
    if(!req.body.rootkey)
    {
       return res.json(200, {err: 'root key required'}); 
    }

    var bip39info = AccountService.impoartAccountBySeedHex( req.body.rootkey );
    res.json(bip39info);
  },

  encryptWallet:function(req,res){
    if( !req.body.keystore)
    {
      return res.json(200, {err: 'keyStore key required',code:-1}); 
    }

    if( !req.body.password )
    {
      return res.json(200, {err: 'Password key required',code:-2});
    }

   
    UserService.encrypt( JSON.stringify(req.body.keystore), req.body.password)
    .then((encryptedkeystore)=>{
      res.json(200,{encryptedkeystore:encryptedkeystore.toString()});
    })
    .catch((exception)=>
    {
      res.json(200,{error:exception});
    });
    
    

  },
    decryptWallet:function(req,res){

    if( !req.body.encryptedkeystore)
    {
      return res.json(200, {err: 'encryptedKeyStore key required'}); 
    }

    if( !req.body.password )
    {
      return res.json(200, {err: 'Password key required'});
    }


      UserService.decrypt( req.body.encryptedkeystore, req.body.password)
      .then((keystore)=>{  
        res.json(200,{keyStore:keystore});
      })
      .catch((error)=>{
         res.json(200,{error:"Password_wrong"});
     });
    
  }

};






