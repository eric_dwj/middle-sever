
/**
 * BalanceController
 * 交易所买卖调用代码 buy,sell
 */

module.exports = {
  orderBook: function (req,res)
  {

       sails.log.debug(req.body);
       if( req.body.market ==null || req.body.market == undefined  )
       {
        return res.json(200, {err: 'market required'}); 
       }
   
       if( req.body.side ==null || req.body.side == undefined )
       {
        return res.json(200, {err: 'side required'}); 
       }

        if( req.body.offset ==null || req.body.offset == undefined  )
       {
        return res.json(200, {err: 'offset required'}); 
       }

        if( req.body.limit ==null || req.body.limit == undefined )
       {
        return res.json(200, {err: 'limit required'}); 
       }
   
       TradeService.orderBook(    
                                  req.body.market,
                                  req.body.side, 
                                  req.body.offset,
                                  req.body.limit
                              )
       .then((result)=>{
        res.json(200, result);
       })
       .catch((exception)=>{
        res.json(200, exception);
       })
  },
  
  getAssetHistory: function (req,res)
  {
       if( !req.body.userid )
       {
        return res.json(200, {err: 'userid required'}); 
       }
   
       TradeService.getAssetHistory(
                                  req.body.userid
                              )
       .then((result)=>{
        res.send(200, result);
       })
       .catch((exception)=>{
        sails.log.error('exception',exception);
        res.json(200, exception);
       })


  },

  getOrder: function (req,res)
  {
       if( !req.body.userid )
       {
        return res.json(200, {err: 'userid required'}); 
       }
   
       TradeService.getOrder(
                                  req.body.userid
                              )
       .then((result)=>{
        sails.log.error('result',result);
        res.json(200, result);
       })
       .catch((exception)=>{
        sails.log.error('exception',exception);
        res.json(200, exception);
       })


  },

  getBalance: function (req,res)
  {
       if( !req.body.userid )
       {
        return res.json(200, {err: 'userid required'}); 
       }
       
   
       TradeService.getBalance(
                                  req.body.userid
                              )
       .then((result)=>{
        sails.log.error('result',result);
        res.json(200, result);
       })
       .catch((exception)=>{
        sails.log.error('exception',exception);
        res.json(200, exception);
       })


  },
  updateBalance: function (req,res)
  {
       if(! req.body.userid)
       {
        return res.json(200, {err: 'userid required'}); 
       }

        if(! req.body.asset)
       {
        return res.json(200, {err: 'asset required'}); 
       }

        if(! req.body.business)
       {
        return res.json(200, {err: 'business required'}); 
       }

        if(! req.body.amount)
       {
        return res.json(200, {err: 'amount required'}); 
       }

       if(! req.body.detail)
       {
        return res.json(200, {err: 'detail required'}); 
       }
   
       TradeService.updateBalance(
                                  req.body.userid, 
                                  req.body.asset, 
                                  req.body.business, 
                                  req.body.amount, 
                                  req.body.detail
                                 )
       .then((result)=>{
        res.json(200, result);
       })
       .catch((exception)=>{
        res.json(200, exception);
       })


  },
	putlimit: function (req, res) {
    // userid,market,side,amount,price
        if(! req.body.userid)
       {
        return res.json(200, {err: 'userid required'}); 
       }

        if(! req.body.market)
       {
        return res.json(200, {err: 'market required'}); 
       }

        if(! req.body.side)
       {
        return res.json(200, {err: 'side required'}); 
       }

        if(! req.body.amount)
       {
        return res.json(200, {err: 'amount required'}); 
       }

       if(! req.body.price)
       {
        return res.json(200, {err: 'price required'}); 
       }

      

       TradeService.putLimit(
                                  req.body.userid, 
                                  req.body.market, 
                                  req.body.side, 
                                  req.body.amount, 
                                  req.body.price                     
                                 )
       .then((result)=>{
        res.json(200, result);
       })
       .catch((exception)=>{
        res.json(200, exception);
       })



	},
  cancel: function (req, res) {

        if(! req.body.userid)
       {
        return res.json(200, {err: 'userid required'}); 
       }

        if(! req.body.market)
       {
        return res.json(200, {err: 'market required'}); 
       }

        if(! req.body.orderid)
       {
        return res.json(200, {err: 'orderid required'}); 
       } 


       TradeService.cancelOrder(
                                  req.body.userid, 
                                  req.body.market, 
                                  req.body.orderid
                                 )
       .then((result)=>{
        res.json(200, result);
       })
       .catch((exception)=>{
        res.json(200, exception);
       })            
  }
	
};

