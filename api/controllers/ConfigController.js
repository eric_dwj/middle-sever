/**
 * ConfigController
 *
 * @description :: Server-side logic for managing configs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    pairs: function(req, res) {
        res.json({pairs:sails.config.trader.pairs});
    }
	
};

