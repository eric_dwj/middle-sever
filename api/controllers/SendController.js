/**
 * SendController
 *
 * @description :: Server-side logic for managing sends
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function(req, res) {

	     if(! req.body.userid)
	     {
	     	return res.json(200, {err: 'userid required'}); 
	     }

	       if(! req.body.assetname)
	     {
	     	return res.json(200, {err: 'assetname required'}); 
	     }

	       if(! req.body.size)
	     {
	     	return res.json(200, {err: 'size required'}); 
	     }

	         if(! req.body.address)
	     {
	     	return res.json(200, {err: 'address required'}); 
	     }

	     var sendObject ={};
	     sendObject.timestamp = new Date().getTime();
	     sendObject.userid    = req.body.userid;
	     sendObject.assetname = req.body.assetname;
	     sendObject.address   = req.body.address;
	     sendObject.size      = parseFloat(req.body.size);
			
		SendService.addSendObjectToSendMQ(sendObject)
		.then((resp)=>{
			return res.json(resp); 
		});
	}
};

