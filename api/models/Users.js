/**
 * Users.js
 *  如果用户忘记了密码，只能通过助记词或者根私钥找回账号，否则
 */

// We don't want to store password with out encryption
var bcrypt = require('bcrypt');

module.exports = {
  
  schema: true,
  
  attributes: {

    userid: {
      type: 'string',
      required:true
    },

    account: {
      type: 'json',
      required: 'true'
    },
    encryptedMnemonic: {
      type: 'string'
    },

    encryptedPassword: {
      type: 'string'
    },
    // We don't wan't to send back encrypted password either
    toJSON: function () {
      var obj = this.toObject();
      delete obj.encryptedPassword;
      return obj;
    }
  },
  // Here we encrypt password before creating a User
  beforeCreate : function (values, next) {
    bcrypt.genSalt(10, function (err, salt) {
      if(err) return next(err);
      bcrypt.hash(values.password, salt, function (err, hash) {
        if(err) return next(err);
        values.encryptedPassword = hash;
        next();
      })
    })
  },

  comparePassword : function (password, user, cb) {
    bcrypt.compare(password, user.encryptedPassword, function (err, match) {

      if(err) cb(err);
      if(match) {
        cb(null, true);
      } else {
        cb(err);
      }
    })
  }
};


