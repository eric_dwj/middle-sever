/**
 * AssetTx.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  

  attributes: {
  	userid: {
      type: 'string',
      required:true
    },
    asset:{
       type:'string',
       required:true
    },
    side:{
    	type:'int',
    	required:true
    },
    amount:{
    	type:'float',
    	required:true
    },
    status:{
    	type:'string',//SUBMITTED, CONFIRMED
    	required:true
    },
    tx:{
       type:'string',
       required:true
    }

  }
};

