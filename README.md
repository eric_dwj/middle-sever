# media
[Sails](http://sailsjs.org) application



# WebToken
https://gist.github.com/vunb/8b6ce5e3da3156adacd64829383e409e

0x1a102c4bc87991502174074a7e020b35d5723745be014f8f23ee7f1a45532921



1，产品特色

1）整合钱包
特点：
采用万能钱包策略，钱包支持多币种；
采用数字签名技术，用户的私钥不会被上传到矿机或者节点上；
采用Bip39和Bip44的钱包技术，可以在通过其他钱包产品，利用助记词恢复钱包；
同时，经过适当的改装，可以将钱包产品改装成硬件钱包；
解决痛点：
目前绝大多数产品需要单一钱包，用户难以统一管理资产；
目前大多数钱包采取热钱包策略，将用户私钥保管于服务器上，造成安全问题；

2）完善的支付平台
特点：
提供统一的虚拟币支付和收款接口，更加容易扩展新的功能；
解决痛点：
目前绝大多数交易所功能单一，直接交由第三方开发，不容易衍生出创新功能

3）可扩展的交易引擎
特点：
采用以redis为核心的中间层server，系统伸缩性强，可以通过hash映射的方式，满足高并发需；
交易引擎可以按照市场为划分，进一步提高系统吞吐性能；
解决痛点：
目前绝大多数交易所不能进行分布式扩展，一旦用户数量到达瓶颈，则必须重新开发平台或者用户体验变差；

4）插拔式的后端服务
特点：
系统可以通过用户的需求，通过各种插拔式接口，快速集成各种服务；
解决痛点：
目前区块链市场并不成熟，各种需求层出不穷，但目前区块链开发框架并不成熟，不能按照市场需求及时形成产品

5）提供量化服务的API
特点：
系统可以提供API服务，用户可以通过JSON RPC调用交易所API，完成量化服务。


2，市场前景

币币交易所专注于数字货币之间的转换，在交易效率和交易成本上有明显的优势。投资者可以直接将手中的数字货币快速的兑换成任何一种，提高了数字货币的流动性。去中心化的钱包，可以让用户更加安全的管理数字资产。将钱包和币币交易结合，可以更加有效的帮助用户管理和兑换数字资产。

支付平台是未来区块链应用的入口，即是流量入口也是虚拟币转移通道，通过钱包和交易所结合，给第三方提供完善的支付服务，就是一个非常具有前景的方向。


